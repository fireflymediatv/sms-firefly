import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class ParticipantService {
   apiUrl = 'https://manager.fireflymedia.tv/api/participants';
  local_url  = 'http://localhost:9000/api/participants';
  constructor(private http: HttpClient) { }

create(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

get(): Observable<any> {
  return this.http.get(`${this.apiUrl}`)
      .map((res: Response) => res)
      .catch(handleError);
}
getCity(): Observable<any> {
  return this.http.get(`${this.apiUrl}/city`)
      .map((res: Response) => res)
      .catch(handleError);
}
getLine(): Observable<any> {
  return this.http.get(`${this.apiUrl}/line`)
      .map((res: Response) => res)
      .catch(handleError);
}

getSurveyCampaign(): Observable<any> {
  return this.http.get(`${this.apiUrl}/surveycampaigns`)
      .map((res: Response) => res)
      .catch(handleError);
}
update(obj): Observable<any> {
  return this.http.put(`${this.apiUrl}/updateparticipant`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}
remove(obj) {
  return this.http.delete(`${this.apiUrl}/${obj.id || obj._id}`)
      .map(() => obj)
      .catch(handleError);
}

// qualified participant
qualifiedParticipant(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/qualifiedparticipant`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

// qualified participant by period
qualifiedParticipantByPeriod(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/qualifiedparticipantbyperiod`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

participantRepartition(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/residencerepartition`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

uploadCsvFile(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/uploadcsvgooglefile`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

nullRepartition(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/nullrepartition`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

autorisedRepartition(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/autorisedrepartition`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

newParticipant(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/getnewparticipant`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

filterByLastMessageDate(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/filterbylastmessagedate`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

searchByCriteria(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/searchcriteria`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

getParticipantResidence(): Observable<any> {
  return this.http.get(`${this.apiUrl}/participantresidence`)
  .map((res: Response) => res)
  .catch(handleError);
}

}
