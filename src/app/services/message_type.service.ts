import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class MessageTypeService {
   apiUrl = 'https://manager.fireflymedia.tv/api/messagetypes';
  local_url  = 'http://localhost:9000/api/messagetypes';
  constructor(private http: HttpClient) { }

  getTypeMessages(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
        .map((res: Response) => res)
        .catch(handleError);
  }
  createTypeMessage(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

}
