import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class GrowthService {
   apiUrl = 'https://manager.fireflymedia.tv/api/growths';
   local_url  = 'http://localhost:9000/api/growths';

  constructor(private http: HttpClient) { }

  getGrowth(): Observable<any> {
    return this.http.get(`${this.apiUrl}/growth`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getGrowthChart(): Observable<any> {
    return this.http.get(`${this.apiUrl}/growthchart`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPhoneAndOpenRepartition(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/phoneandopen`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getGrowthPeriod(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/getperiod`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

}
