import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class PrintService {
   apiUrl = 'https://manager.fireflymedia.tv/api/prints';
  local_url  = 'http://localhost:9000/api/prints';
  constructor(private http: HttpClient) { }

  getPrints(): Observable<any> {
    return this.http.get(`${this.apiUrl}/list/PLANIFIED`)
        .map((res: Response) => res)
        .catch(handleError);
  }

}
