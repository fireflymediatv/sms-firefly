import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class UssdreceiveService {
   apiUrl = 'https://manager.fireflymedia.tv/api/ussdreceives';
  local_url  = 'http://localhost:9000/api/ussdreceives';
  constructor(private http: HttpClient) { }

  getSmsreceive(): Observable<any> {
    return this.http.get(`${this.apiUrl}/all`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getSmsByDate(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/getsmsbydate`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getSmsChart(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/getsmschart`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  exportSmsToCsv(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/exportsmstocsv`, obj)
        .map((res: Response) => res)
        .catch(handleError);
}

spentSms(obj): Observable<any>  {
  return this.http.post(`${this.apiUrl}/spentsms`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

}
