import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class SurveyCampaignService {
   apiUrl = 'https://manager.fireflymedia.tv/api/surveycampaigns';
  local_url  = 'http://localhost:9000/api/surveycampaigns';
  constructor(private http: HttpClient) { }

  getSurveyCampaign(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
        .map((res: Response) => res)
        .catch(handleError);
  }
  getUssdSurveyCampaign(): Observable<any> {
    return this.http.get(`${this.apiUrl}/getallussd`)
        .map((res: Response) => res)
        .catch(handleError);
  }
  getAllCustomer(): Observable<any> {
    return this.http.get(`${this.apiUrl}/getallcustomer`)
        .map((res: Response) => res)
        .catch(handleError);
  }

createSurvey(obj): Observable<any>  {
  return this.http.post(`${this.apiUrl}/survey`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

stopCampaign(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/stopsurvey`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

reloadCampaign(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/reloadsurvey`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

getActiveCustomers(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/activecustomer`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}
get(id): Observable<any> {
  return this.http.get(`${this.apiUrl}/${id}`)
      .catch(handleError);
}

boostOrRemakeCampaign(obj): Observable<any> {
  return this.http.post(`${this.apiUrl}/boostorremake`, obj)
      .map((res: Response) => res)
      .catch(handleError);
}

remove(obj) {
  return this.http.delete(`${this.apiUrl}/${obj.id || obj._id}`)
      .map(() => obj)
      .catch(handleError);
}

}
