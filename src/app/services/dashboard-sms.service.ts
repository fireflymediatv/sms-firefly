import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class DashboardSmsService {
   apiUrl = 'https://manager.fireflymedia.tv/api';
  local_url  = 'http://localhost:9000/api';
  constructor(private http: HttpClient) { }

  getOpendevices(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/status/lastweek`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehicles(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehicles`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getEstimatedResult(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/estimatedResult`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehiclesWithStickers(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehiclestickers`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehiclesFL(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehiclesfl`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalProbes(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/proberequests/totalprobes`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalOpen(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/totalopen`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getUsersInTheBus(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/proberequests/usersinthebus`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalParticipant(): Observable<any> {
    return this.http.get(`${this.apiUrl}/participants/countparticipant`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalParticipantWeek(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/participated`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalSmsreceived(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/countsmsreceived`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalUniqueBus(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/busunique`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getAllSurveyCampaigns(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/surveycampaigns/activesurveycampaigns`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }


  // All Endpoints for Suivi-Campaign only
  getOpendeviceByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/bycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalPopByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/totalpopbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehicleByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehiclebycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehiclesWithStickerByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehiclestickerbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getPrintVehiclesFLByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/activevehiclesflbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  getTotalUniqueBusByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/busuniquebycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalProbeByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/proberequests/totalprobebycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getUsersInTheBusByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/proberequests/usersinthebusbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalParticipantWeekByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/participatedbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getTotalSmsreceivedByCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/countsmsreceivedbycampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  selectedCampaign(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/surveycampaigns/onselectedcampaign`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }


  // All endpoint for Combination Chart
  ChartActivationNumber(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/activationnumber`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartActivationNumberOnRapport(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/activationnumberonrapport`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartActivationNewNumber(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/smsreceives/activationnewnumber`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartActivationPop(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/activationpop`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartActivationDevice(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/devices/activationdevice`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartStickedBus(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/prints/stickedbus`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  // endpoint for other chart
  ChartGender(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/participants/chartgender`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartAge(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/participants/chartage`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }
  ChartJob(obj): Observable<any> {
        return this.http.post(`${this.apiUrl}/participants/chartjob`, obj)
            .map((res: Response) => res)
            .catch(handleError);
      }

      participantGenderRepartition(obj): Observable<any> {
        return this.http.post(`${this.apiUrl}/participants/genderrepartition`, obj)
            .map((res: Response) => res)
            .catch(handleError);
      }
}
