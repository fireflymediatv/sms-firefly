import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class AudienceService {
  apiUrl = 'https://manager.fireflymedia.tv/api/kpis';
  local_url  = 'http://localhost:9000/api/kpis';
  constructor(private http: HttpClient) { }

  getKpis(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getChartAudiences(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/chartaudience`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

}
