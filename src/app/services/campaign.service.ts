import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class CampaignService {
   apiUrl = 'https://manager.fireflymedia.tv/api/campaigns';
  local_url  = 'http://localhost:9000/api/campaigns';
  constructor(private http: HttpClient) { }

  getCampaigns(): Observable<any> {
    return this.http.get(`${this.apiUrl}/list/smscampaign`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  remove(obj) {
    return this.http.delete(`${this.apiUrl}/${obj.id || obj._id}`)
        .map(() => obj)
        .catch(handleError);
  }

}
