import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class MessageService {
   apiUrl = 'https://manager.fireflymedia.tv/api/messages';
  local_url  = 'http://localhost:9000/api/messages';
  constructor(private http: HttpClient) { }

  getMessages(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getMessagetype(): Observable<any> {
    return this.http.get(`${this.apiUrl}/messagetype`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getSurveyCampaign(): Observable<any> {
    return this.http.get(`${this.apiUrl}/surveycampaigns`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  getResponseType(): Observable<any> {
    return this.http.get(`${this.apiUrl}/responsetype`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  createMessage(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  listMessageOfResponse(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/listmsg`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  update(obj): Observable<any> {
    return this.http.post(`${this.apiUrl}/updatemessage`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  remove(obj) {
    return this.http.delete(`${this.apiUrl}/${obj.id || obj._id}`)
        .map(() => obj)
        .catch(handleError);
  }

}
