import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

function handleError(err) {
  return Observable.throw(err.error || 'Server error');
}


@Injectable({
  providedIn: 'root',
})
export class ResponseService {
   apiUrl = 'https://manager.fireflymedia.tv/api/responses';
  local_url  = 'http://localhost:9000/api/responses';
  constructor(private http: HttpClient) { }

  getResponses(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
        .map((res: Response) => res)
        .catch(handleError);
  }

  createResponse(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  update(obj): Observable<any>  {
    return this.http.post(`${this.apiUrl}/updateresponse`, obj)
        .map((res: Response) => res)
        .catch(handleError);
  }

  remove(obj) {
    return this.http.delete(`${this.apiUrl}/${obj.id || obj._id}`)
        .map(() => obj)
        .catch(handleError);
  }

}
