import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';
const roleUser = "user"
@Injectable()
export class AuthGuard implements CanActivate {
  currentUser;
    constructor(private authService: NbAuthService, private router: Router) {
    }
  
    canActivate() {
      this.currentUser = JSON.parse(localStorage.getItem('user'));
      return this.authService.isAuthenticated()
        .pipe(
          tap(authenticated => {
            if (!authenticated) {
              this.router.navigate(['auth/login']);
            }
               
          }),
        );
    } 


  }