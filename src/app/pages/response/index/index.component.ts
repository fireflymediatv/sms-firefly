import { TemplateRef, ViewChild, Component, OnInit, ChangeDetectionStrategy, ElementRef } from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { ResponseService } from '../../../services/response.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ResponseService],
})
export class IndexResponseComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public responses;
  public data;
  popoverTitle = 'Attention';
  popoverMessage = 'Êtes-vous sûr de vouloir effectuer cette opération?';
  confirmClicked = false;
  cancelClicked = false;
  @ViewChild('_idTpl', { static: true }) _idTpl: TemplateRef<any>;
  @ViewChild('responseTpl', { static: true }) responseTpl: TemplateRef<any>;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;

  @ViewChild('response') response: ElementRef<any>;
  @ViewChild('_id') _id: ElementRef<any>;
  statuses: NbComponentStatus[] = ['primary'];
  selectRow: number;
  currentUser;
  tabValue = [];
  constructor(private responseservice: ResponseService,
    private router: Router,
    private route: ActivatedRoute) {
    this.router = router;
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [
      { key: 'message_id.wording', title: 'Message' },
      { key: 'message_id.order', title: 'Position' },
      { key: 'response', title: 'Réponses', cellTemplate: this.responseTpl },
      { key: 'message_id.survey_campaign_id.name', title: 'Campagne' },
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getResponses();
  }

  // Show All message
  public getResponses() {
    this.responseservice.getResponses().subscribe((data: Array<object>) => {
      //console.log(' Sms response services', data);
      this.configuration.isLoading = false;
      this.responses = data;
    });
  }

  edit(rowIndex: number): void {
    this.selectRow = rowIndex;
  }

  update(val): void {
    this.responses = [
      ...this.responses.map((obj, index) => {
        if (index === this.selectRow) {
          return {
            _id: val._id,
            response: this.response.nativeElement.value,
          };
        }
        // console.log('object modified apres', obj._id);
        return obj;
      }),
    ];

    //console.log('data modified Avant', this.data[this.selectRow]);
    this.route.params.subscribe(params => {
      this.responseservice.update(this.responses[this.selectRow]).subscribe(res => {
        //console.log('data modified apres', this.data[this.selectRow]);
        //    this.cdr.detectChanges()
        this.getResponses();
      });

    });
    this.selectRow = -1;

  }

  removeResponse(val) {
    //console.log('val to remove', val);
    this.route.params.subscribe(params => {
      this.responseservice.remove(val).subscribe(res => {
        this.getResponses();
      });
    });
  }

}
