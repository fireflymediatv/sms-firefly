import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MessageService } from '../../../services/message.service';
import { SurveyCampaignService} from '../../../services/surveycampaign.service';
import { ResponseService} from '../../../services/response.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers : [MessageService, ResponseService, SurveyCampaignService],
})
export class AddResponseComponent implements OnInit {
  responseForm: FormGroup;
  submittedResponse = false;
  responses;
  messages;
  surveycampaigns;
  survey_id;
  survey_selected = false;
  message_selected ;
  test;
  message_id;

  constructor(private fb: FormBuilder,
    private surveycampaignservice: SurveyCampaignService,
    private messageservice: MessageService,
    private responseservice: ResponseService,
    private router: Router,
    private route: ActivatedRoute ) {
this.responseForm = this.fb.group({
      response : ['', Validators.required],
      order : ['', Validators.required],
      survey_campaign_id : ['', Validators.required],
      });
      this.test = 'testons'
}

  ngOnInit(): void {
    this.getSurveyCampaigns();
  }

  // convenience getter for easy access to form fields
  get fResponse() {
    return this.responseForm.controls;
  }

   // Submit form
   onSubmitResponse() {
    this.submittedResponse = true;
    console.log('form response', this.fResponse);
    if (this.responseForm.invalid) {
      console.log('error Message form');
      return this.responseForm.controls; ;
    }
    let form_tResponse = this.fResponse.response.parent.value;

    Object.defineProperty(form_tResponse, 'message_id', {value: this.message_id, enumerable: true,
      configurable: true});
    console.log('final response value', form_tResponse );

    this.responseservice.createResponse(form_tResponse).subscribe((data: Array<object>) => {
      this.fResponse.response.reset();
      console.log(' successfully create response');
      this.router.navigate(['pages/response']);
   });
  }

   // Show All message
   public getMessages() {
    this.messageservice.getMessages().subscribe((data: Array<object>) => {
       console.log(' Sms messages services', data);
       this.messages = data;
    });
  }

  // Show All Survey campaigns
  public getSurveyCampaigns() {
    this.surveycampaignservice.getSurveyCampaign().subscribe((data: Array<object>) => {
       console.log(' survey campaign services', data);
       this.surveycampaigns = data;
    });
  }

  toggleSurvey(survey_id) {
    console.log(' Click change ', survey_id);
    this.survey_selected = true;
    this.messageservice.listMessageOfResponse({'survey_id': survey_id}).subscribe((data: Array<object>) => {
      console.log(' list msg ', data);
      this.messages = data;
   });
  }

  toggleMessage(wording) {
    console.log(' Click change order ', wording);
    let split_message = wording.split('--')
    this.message_selected = split_message[0];
    this.message_id = split_message[1];
  }

  ngDoCheck() {
    this.message_selected = this.message_selected;
  }

}
