import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexResponseComponent } from './index/index.component';
import { AddResponseComponent } from './add/add.component';
import { GetResponseComponent } from './get/get.component';
import { EditResponseComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'response', component: IndexResponseComponent },
  { path: 'response/add', component: AddResponseComponent },
  { path: 'response/edit/:id', component: EditResponseComponent },
  { path: 'response/:id', component: GetResponseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResponseRoutingModule { }

export const routedComponents = [
  IndexResponseComponent,
  AddResponseComponent ,
  EditResponseComponent,
  GetResponseComponent,
];
