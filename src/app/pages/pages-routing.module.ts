import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IndexComponent } from './campaign/index/index.component';
import { AddComponent } from './campaign/add/add.component';
import { AddKeywordComponent } from './keyword/add/add.component';
import { EditKeywordComponent } from './keyword/edit/edit.component';
import { GetKeywordComponent } from './keyword/get/get.component';
import { IndexKeywordComponent } from './keyword/index/index.component';
import { AddParticipantComponent } from './participant/add/add.component';
import { IndexParticipantComponent } from './participant/index/index.component';
import { IndexDashboardComponent } from './dashboard-sms/index/index.component';
import { IndexSuiviComponent } from './suivi-campaign/index/index.component';
import { IndexRapportComponent } from './rapport/index/index.component';
import { AddMessageComponent } from './message/add/add.component';
import { EditMessageComponent } from './message/edit/edit.component';
import { GetMessageComponent } from './message/get/get.component';
import { IndexMessageComponent } from './message/index/index.component';

import { IndexCustomerComponent } from './customer/index/index.component';
import { UssdreceiveComponent } from './ussdreceive/ussdreceive.component';
import { GetCustomerComponent } from './customer/get/get.component';
import { AddResponseComponent } from './response/add/add.component';
import { EditResponseComponent } from './response/edit/edit.component';
import { GetResponseComponent } from './response/get/get.component';
import { IndexResponseComponent } from './response/index/index.component';
import { SmsreceiveComponent } from './smsreceive/smsreceive.component';
import { AudienceComponent } from './audience/audience.component';
import { GrowthComponent } from './growth/growth.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AuthGuard } from '../auth-guard.service';
import { IndexUploadParticipantComponent } from './uploadparticipant/index/index.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    },
    {path: 'dashboard-sms', canActivate: [AuthGuard], component: IndexDashboardComponent},
    {path: 'suivi-campaign', canActivate: [AuthGuard], component: IndexSuiviComponent},
    {path: 'rapport', canActivate: [AuthGuard], component: IndexRapportComponent},
    {path: 'uploadparticipant', canActivate: [AuthGuard], component: IndexUploadParticipantComponent},
    {
      path: 'smsreceive',
      canActivate: [AuthGuard],
      component: SmsreceiveComponent,
    },
    {
      path: 'audience',
      canActivate: [AuthGuard],
      component: AudienceComponent,
    },
    {
      path: 'growth',
      canActivate: [AuthGuard],
      component: GrowthComponent,
    },
    {
      path: 'ussdreceive',
      canActivate: [AuthGuard],
      component: UssdreceiveComponent,
    },

    {
      path: 'customer',
      canActivate: [AuthGuard],
      component: IndexCustomerComponent,
    },
    {
      path: 'customer/:id',
      canActivate: [AuthGuard],
      component: GetCustomerComponent,
    },
    {path: 'campaign', canActivate: [AuthGuard], component: IndexComponent},
    {path: 'campaign/add', canActivate: [AuthGuard], component: AddComponent},
    {path: 'keyword', canActivate: [AuthGuard], component: IndexKeywordComponent},
    {path: 'participant', canActivate: [AuthGuard], component: IndexParticipantComponent},
    {path: 'participant/add', canActivate: [AuthGuard], component: AddParticipantComponent},
    {path: 'keyword/add', canActivate: [AuthGuard], component: AddKeywordComponent},
    {path: 'message', canActivate: [AuthGuard], component: IndexMessageComponent},
    {path: 'message/add', canActivate: [AuthGuard], component: AddMessageComponent},
    {path: 'response', canActivate: [AuthGuard], component: IndexResponseComponent},
    {path: 'response/add', canActivate: [AuthGuard], component: AddResponseComponent},
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
    },
    {
      path: 'modal-overlays',
      loadChildren: () => import('./modal-overlays/modal-overlays.module')
        .then(m => m.ModalOverlaysModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },
    {
      path: 'editors',
      loadChildren: () => import('./editors/editors.module')
        .then(m => m.EditorsModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard-sms',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
