import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from '@angular/router';
import { FusionChartsModule } from 'angular-fusioncharts';
// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
// Add dependencies to FusionChartsModule
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

import { IndexDashboardComponent } from './index/index.component';
// import { AddParticipantComponent } from './add/add.component';
// import { GetKeywordComponent } from './get/get.component';
// import { EditKeywordComponent } from './edit/edit.component';

export const dashboardRoute: Routes = [
  { path: 'dashboard-sms', component: IndexDashboardComponent },
  // { path: 'participant/add', component: AddParticipantComponent },
  // { path: 'keyword/edit/:id', component: EditKeywordComponent },
  // { path: 'keyword/:id', component: GetKeywordComponent }
];
import { DashboardSmsRoutingModule, routedComponents   } from './dashboard-sms-routing.module';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    FusionChartsModule,
    TableModule,
    DashboardSmsRoutingModule,
  ],
})
export class DashboardSmsModule { }
