import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexDashboardComponent } from './index/index.component';
// import { AddParticipantComponent } from './add/add.component';
// import { GetKeywordComponent } from './get/get.component';
// import { EditKeywordComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'dashboard-sms', component: IndexDashboardComponent },
  // { path: 'participant/add', component: AddParticipantComponent },
  // { path: 'keyword/edit/:id', component: EditKeywordComponent },
  // { path: 'keyword/:id', component: GetKeywordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardSmsRoutingModule { }

export const routedComponents = [
  IndexDashboardComponent,
  // AddParticipantComponent ,
  // EditKeywordComponent,
  // GetKeywordComponent
];
