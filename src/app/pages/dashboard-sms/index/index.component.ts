import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { DashboardSmsService } from '../../../services/dashboard-sms.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { ParticipantService } from '../../../services/participant.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [DashboardSmsService, ParticipantService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexDashboardComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public probe;
  public data;
  dateSmsForm: FormGroup;
  print;
  printStickers;
  printFL;
  user_in_the_bus;
  total_open;
  total_participant;
  total_participant_week;
  total_sms_received;
  sticker_user;
  print_user;
  public estimatedResult: any = {};
  public avgProbe;
  loading_total_participant = false;
  loading_total_participant_week = false;
  loading_print_user = false;
  loading_total_open = false;
  loading_user_in_the_bus = false;
  loading_printFL = false;
  loading_printStickers = false;
  loading_print = false;
  loading_data = false;
  loading_probe = false;
  loading_newParticipant = false;
  loading_total_sms_received = false;
  loading_total_bus_unique = false;
  total_bus_unique;
  total_survey;
  diffInDays;
  firstDate;
  secondDate;
  hideDataOnSubmit = false;
  loading_sticker_user = false;
  submittedDate = false;
   date;
  compareProbe;
  pathImgProbe;
  colorProbe;
  compareUserInTheBus;
  pathImgUserInTheBus;
  colorUserInTheBus;
  compareSmsReceive;
  pathImgSmsReceive;
  colorSmsReceive;
  compareParticipantWeek;
  pathImgParticipantWeek;
  colorParticipantWeek;
  comparePop;
  pathImgPop;
  colorPop;
  comparePrint;
  pathImgPrint;
  colorPrint;
  compareData;
  pathImgData;
  colorData;
  compareBusUnique;
  pathImgBusUnique;
  colorBusUnique;
  compareEstimatedResult;
  pathImgEstimatedResult;
  colorEstimatedResult;
  finalEstimate;
  newParticipant;
  printUsers;
  comparePrintUsers;
  pathImgPrintUsers;
  colorPrintUsers;
  finalPrintUsers;
  loading_estimatedResult = false;
  allUsersInTheBus;
  currentUser;
   spaces = price => String(price)
  .replace(
    /(?!^)(?=(?:\d{3})+$)/g,
    ' ',
  )
  residenceRepartition;
  dataSourceResidence;
  dataSourceAge;
  dataSourceAllGender;
  dataSourceJob;
  dataSourceLine;
  chartConfigJob: Object;
  chartConfigResidence: Object;
  chartConfigLine: Object;
  chartConfigAllGender: Object;
  chartConfigAge: Object;
  jobRepartition;
  ageRepartition;
  lineRepartition;
  allParticipantGenderRepartition;
  max: Date;

  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;


  constructor(private dashboardSmsService:  DashboardSmsService,
              private participantService:  ParticipantService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private cdr: ChangeDetectorRef) {
              //  this.router = router;
                this.dateSmsForm = this.fb.group({
                  data_date_sms : ['', Validators.required],
                 });
                 this.max = new Date();
               }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.getTotalProbes();
    this.getTotalBusUnique();
    this.getOpenDevices();
    this.getPrintVehicles();
    this.getPrintVehiclesWithStickers();
    this.getPrintVehiclesWithFL();
    this.getTotalOpen();
    this.getTotalPartcipant();
    this.getTotalPartcipantWeek();
    this.getUsersInTheBus();
    this.getTotalSmsReceived();
    this.getAllSurveyCampaigns();
    this.getParticipantRepartition();
    this.getAllGenderRepartition();
    this.getNewParticipant()
  }

  // convenience getter for easy access to form fields
  get fDateProbe() {
    return this.dateSmsForm.controls;
  }

  onSubmitDateInterval() {
          this.submittedDate = true;
          if (this.dateSmsForm.invalid) {
            console.log('error date');
            return this.dateSmsForm.controls;
          }
          this.date = {
            'start_date' : this.datePipe.transform(this.fDateProbe.data_date_sms.value.start, 'yyyy-MM-dd'),
              'end_date' : this.datePipe.transform(this.fDateProbe.data_date_sms.value.end, 'yyyy-MM-dd')
          };
          console.log('date interval', this.date)
          this.firstDate = moment(this.date.start_date);
this.secondDate = moment(this.date.end_date);
this.diffInDays = Math.abs(this.firstDate.diff(this.secondDate, 'days'));

         this.getTotalProbes();
         this.getTotalBusUnique();
         this.getOpenDevices();
         this.getPrintVehicles();
         this.getTotalOpen();
         this.getPrintVehiclesWithStickers();
         this.getPrintVehiclesWithFL();
         this.getTotalPartcipantWeek();
         this.getTotalSmsReceived();
         this.getUsersInTheBus();
         this.getNewParticipant();
  }

  public getTotalBusUnique() {
    this.loading_total_bus_unique = true;
    this.dashboardSmsService.getTotalUniqueBus(this.date).subscribe((res: Array<object>) => {
       this.total_bus_unique = res[0];
       this.compareBusUnique = res[1];
       this.pathImgBusUnique = res[2];
       this.colorBusUnique = res[3];
       this.estimatedResult = res[4];
       this.finalEstimate = this.estimatedResult[0];
       this.compareEstimatedResult = this.estimatedResult[1];
       this.pathImgEstimatedResult = this.estimatedResult[2];
       this.colorEstimatedResult = this.estimatedResult[3];
       this.loading_total_bus_unique = false;
    });
  }
   // Show All Sms partner
   public getOpenDevices() {
     this.loading_data = true;
    this.dashboardSmsService.getOpendevices(this.date).subscribe((data: Array<object>) => {
       this.data = data[0];
       this.compareData = data[1];
       this.pathImgData = data[2];
       this.colorData = data[3];
       this.loading_data = false;
    });
  }

  public getPrintVehicles() {
    this.loading_print = true;
    this.dashboardSmsService.getPrintVehicles(this.date).subscribe((res: Array<object>) => {
       this.print = res[0];
       this.comparePrint = res[1];
       this.pathImgPrint = res[2];
       this.colorPrint = res[3];
       this.printUsers = res[4];
       this.finalPrintUsers = this.printUsers[0];
       this.comparePrintUsers = this.printUsers[1];
       this.pathImgPrintUsers = this.printUsers[2];
       this.colorPrintUsers = this.printUsers[3];
       this.loading_print = false;
       // console.log('color', this.colorPrintUsers);
    });
  }

  public getPrintVehiclesWithStickers() {
    this.loading_printStickers = true;
    this.dashboardSmsService.getPrintVehiclesWithStickers(this.date).subscribe((res: Array<object>) => {
       this.printStickers = res;
       this.loading_printStickers = false;
    });
  }

  public getNewParticipant() {
    this.loading_newParticipant = true;
    this.participantService.newParticipant(this.date).subscribe((res: Array<object>) => {
       this.newParticipant = res;
       this.loading_newParticipant = false;
    });
  }

  public getPrintVehiclesWithFL() {
    this.loading_printFL = true;
    this.dashboardSmsService.getPrintVehiclesFL(this.date).subscribe((res: Array<object>) => {
       this.printFL = res;
       this.loading_printFL = false;
    });
  }

  public getTotalProbes() {
    this.loading_probe = true;
    // this.loading_print_user = true;
    this.dashboardSmsService.getTotalProbes(this.date).subscribe((res: Array<object>) => {
       this.probe = res[0];
       this.avgProbe = res[1];
       this.compareProbe = res[2];
       this.pathImgProbe = res[3];
       this.colorProbe = res[4];
       this.loading_probe = false;

       // this.print_user = this.probe * this.print;
       // this.loading_print_user = false;
       // console.log('lenght', this.estimatedResult);
    });
  }

  public getTotalOpen() {
    this.loading_total_open = true;
    this.dashboardSmsService.getTotalOpen(this.date).subscribe((res: Array<object>) => {
       this.total_open = res[0];
       this.comparePop = res[1];
       this.pathImgPop = res[2];
       this.colorPop = res[3];
       this.loading_total_open = false;
    });
  }

  public getUsersInTheBus() {
    this.loading_user_in_the_bus = true;
    this.loading_sticker_user = true;

    this.dashboardSmsService.getUsersInTheBus(this.date).subscribe((res: Array<object>) => {
       this.user_in_the_bus = res[0];
       this.compareUserInTheBus = res[1];
       this.pathImgUserInTheBus = res[2];
       this.colorUserInTheBus = res[3];
       this.allUsersInTheBus = res[4];
       this.loading_user_in_the_bus = false;

       this.sticker_user = ((this.user_in_the_bus / 2) * this.printStickers).toFixed(0);
       this.loading_sticker_user = false;


    });
  }

  public getTotalPartcipant() {
    this.loading_total_participant = true;
    this.dashboardSmsService.getTotalParticipant().subscribe((res: Array<object>) => {
       this.total_participant = res;
       this.loading_total_participant = false;
    });
  }

  public getTotalPartcipantWeek() {
    this.loading_total_participant_week = true;
    this.dashboardSmsService.getTotalParticipantWeek(this.date).subscribe((res: Array<object>) => {
       this.total_participant_week = res[0];
       this.compareParticipantWeek = res[1];
       this.pathImgParticipantWeek = res[2];
       this.colorParticipantWeek = res[3];
       this.loading_total_participant_week = false;
    });
  }

  public getTotalSmsReceived() {
    this.loading_total_sms_received = true;
    this.dashboardSmsService.getTotalSmsreceived(this.date).subscribe((res: Array<object>) => {
       this.total_sms_received = res[0];
       this.compareSmsReceive = res[1];
       this.pathImgSmsReceive = res[2];
       this.colorSmsReceive = res[3];
       this.loading_total_sms_received = false;
    });
  }

  public getAllSurveyCampaigns() {
    this.dashboardSmsService.getAllSurveyCampaigns(this.date).subscribe((res: Array<object>) => {
       this.total_survey = res;
    });
  }

  public getAllGenderRepartition() {
    let obj;
    this.dashboardSmsService.participantGenderRepartition(obj).subscribe((res: Array<object>) => {
       this.allParticipantGenderRepartition = res[0];
       // console.log('genre', this.allParticipantGenderRepartition);
       this.chartAllGenderRepartition();
    });
  }
  public getParticipantRepartition() {
    let obj;
    this.participantService.participantRepartition(obj).subscribe((data: Array<object>) => {
       // console.log(' participants repartitions', data);
       // this.configuration.isLoading = false;
       this.residenceRepartition = data[0];
       this.jobRepartition = data[1];
       this.ageRepartition = data[2];
       this.lineRepartition = data[3];
       // console.log(' participants repartitions', this.residenceRepartition);
       this.chartResidenceRepartition();
       this.chartAgeRepartition();
       this.chartJobRepartition();
       this.chartLineRepartition();
    });
  }

  chartResidenceRepartition() {
   // console.log('enter chart', this.residenceRepartition);
    // fusion chart
    this.chartConfigResidence = {
     width: '900',
     height: '400',
     type: 'column2d',
     dataFormat: 'json',
  };

  this.dataSourceResidence = {
     'chart': {
      caption: 'Répartition par lieu de résidence',
      subcaption: '',
      yaxisname: 'Habitants',
      xaxisname: 'Lieu de résidence',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.residenceRepartition,
   };
  }

  chartAgeRepartition() {
   // console.log('enter chart', this.residenceRepartition);
    // fusion chart
    this.chartConfigAge = {
     width: '450',
     height: '400',
     type: 'bar2d',
     dataFormat: 'json',
  };

  this.dataSourceAge = {
     'chart': {
      caption: 'Répartition par âge',
      subcaption: '',
      yaxisname: 'Nombre de personnes',
      xaxisname: 'Tranche d\'âge',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.ageRepartition,
   };
  }

  chartJobRepartition() {
    // console.log('enter chart', this.residenceRepartition);
     // fusion chart
     this.chartConfigJob = {
      width: '450',
      height: '400',
      type: 'pie2d',
      dataFormat: 'json',
   };

   this.dataSourceJob = {
    'chart': {
      'caption': 'Répartition par métiers',
      'subCaption': '',
      'numberPrefix': '',
      'showPercentInTooltip': '0',
      'decimals': '1',
      'useDataPlotColorForLabels': '1',
      // Theme
      'theme': 'fusion',

    },
    'data': this.jobRepartition,
    };
   }

   chartAllGenderRepartition() {
    // console.log('enter chart', this.residenceRepartition);
     // fusion chart
     this.chartConfigAllGender = {
      width: '900',
      height: '400',
      type: 'pie2d',
      dataFormat: 'json',
   };

   this.dataSourceAllGender = {
    'chart': {
      'caption': 'Répartition par genre',
      'subCaption': '',
      'numberPrefix': '',
      'showPercentInTooltip': '0',
      'decimals': '1',
      'useDataPlotColorForLabels': '1',
      // Theme
      'theme': 'fusion',

    },
    'data': this.allParticipantGenderRepartition,
    };
   }
   chartLineRepartition() {
    // console.log('enter chart', this.residenceRepartition);
     // fusion chart
     this.chartConfigLine = {
      width: '1000',
      height: '400',
      type: 'column2d',
      dataFormat: 'json',
   };
   this.dataSourceLine = {
    'chart': {
      'caption': 'Répartition par Ligne',
      'subCaption': '',
      'numberPrefix': '',
      'showPercentInTooltip': '0',
      'decimals': '1',
      'useDataPlotColorForLabels': '1',
      // Theme
      'theme': 'fusion',

    },
    'data': this.lineRepartition,
    };
   }

}
