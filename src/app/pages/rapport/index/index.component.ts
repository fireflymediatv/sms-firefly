import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { DashboardSmsService } from '../../../services/dashboard-sms.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus, NbThemeService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [DashboardSmsService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexRapportComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public probe: any = 0;
  public data;
  dateSmsForm: FormGroup;
  print;
  menu;
  printStickers;
  printFL;
  user_in_the_bus: any = 0;
  total_open: any = 0;
  total_participant;
  fileName = [];
  total_participant_week: any = 0;
  total_sms_received: any = 0;
  sticker_user: any = 0;
  print_user: any = 0;
  public estimatedResult;
  public avgProbe;
  loading_total_participant = false;
  loading_total_participant_week = false;
  loading_print_user = false;
  loading_total_open = false;
  loading_user_in_the_bus = false;
  loading_printFL = false;
  loading_printStickers = false;
  loading_print = false;
  loading_data = false;
  loading_probe = false;
  loading_total_sms_received = false;
  loading_total_bus_unique = false;
  total_bus_unique;
  total_survey;
  diffInDays;
  selected = false;
  firstDate;
  surveyInfo;
  secondDate;
  hideDataOnSubmit = false;
  loading_sticker_user = false;
  submittedDate = false;
   date;
   jobRepartition;
   lineRepartition;
   allUserInTheBus;
   selectCampaign;
   activationNumber;
   activationNewNumber
  activationDate;
  activationPop;
  activationDevice;
  stickedBus;
  chartGenderLegend;
  chartGenderValue;
  chartAgeValue;
  options: any = {};
  themeSubscription: any;
  chartConfigGender: Object;
      dataSourceGender: {};
      chartConfigAge: Object;
      dataSourceAge: {};
      chartConfigJob: Object;
    dataSourceJob: {};
    chartConfigLine: Object;
    dataSourceLine: {};
    chartConfigActivation: Object;
    dataSourceActivation:{};
    chartConfigNewNumber: Object;
    dataSourceNewNumber:{};
  editRow: number;
   spaces = price => String(price)
  .replace(
    /(?!^)(?=(?:\d{3})+$)/g,
    ' ',
  )
  width;
  height;
  type;
  dataFormat;
  dataSource;
  currentUser;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;


  constructor(private dashboardSmsService:  DashboardSmsService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private theme: NbThemeService,
              private cdr: ChangeDetectorRef) {
                this.router = router;
                this.dateSmsForm = this.fb.group({
                  campaign : ['', Validators.required],
                 });
               }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.getAllSurveyCampaigns();
  }

  // convenience getter for easy access to form fields
  get fDateProbe() {
    return this.dateSmsForm.controls;
  }

  onSubmitDateInterval() {
          this.submittedDate = true;
          if (this.dateSmsForm.invalid) {
            console.log('error date');
            return this.dateSmsForm.controls;
          }
          this.date = {
              'campaign' : this.fDateProbe.campaign.value,
          };
          this.chartJob();
         this.chartGender();
         this.chartAge();
         this.getTotalProbeByCampaign();
         this.getTotalBusUniqueByCampaign();
         this.getOpenDeviceByCampaign();
         this.getTotalPopByCampaign();
         this.getPrintVehicleByCampaign();
         this.getPrintVehiclesWithStickerByCampaign();
         this.getPrintVehiclesWithFLByCampaign();
         this.getTotalPartcipantWeekByCampaign();
         this.getTotalSmsReceivedByCampaign();
         this.getUsersInTheBusByCampaign();
        // this.chartActivationPop();
         this.chartActivationNumber();
         this.chartActivationNewNumber()
         //this.chartActivationDevice();
         this.chartStickedBus();
  }

  public getTotalProbeByCampaign() {
    this.loading_probe = true;
    this.dashboardSmsService.getTotalProbeByCampaign(this.date).subscribe(async (res: Array<object>) => {
       this.probe = res[0];
       this.avgProbe = res[1];
       this.loading_probe = false;
       // console.log('lenght', this.estimatedResult);
    });
  }

  public getTotalBusUniqueByCampaign() {
    this.loading_total_bus_unique = true;
    this.dashboardSmsService.getTotalUniqueBusByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_bus_unique = res[0];
       this.estimatedResult = res[1];
       this.loading_total_bus_unique = false;
    });
  }
   // Show All Sms partner
   public getOpenDeviceByCampaign() {
     this.loading_data = true;
    this.dashboardSmsService.getOpendeviceByCampaign(this.date).subscribe((data: Array<object>) => {
       this.data = data;
       this.loading_data = false;
    });
  }

  public getPrintVehicleByCampaign() {
    this.loading_print = true;
    this.loading_print_user = true;
    this.dashboardSmsService.getPrintVehicleByCampaign(this.date).subscribe((res: Array<object>) => {
      this.print = res[0];
      this.print_user = res[1];
      this.loading_print_user = false;
      this.loading_print = false;
    });
  }

  public getPrintVehiclesWithStickerByCampaign() {
    this.loading_printStickers = true;
    this.loading_sticker_user = true;
    this.dashboardSmsService.getPrintVehiclesWithStickerByCampaign(this.date).subscribe((res: Array<object>) => {
      this.printStickers = res[0];
      this.sticker_user = res[1];
      this.loading_sticker_user = false;
      this.loading_printStickers = false;
    });
  }

  public getPrintVehiclesWithFLByCampaign() {
    this.loading_printFL = true;
    this.dashboardSmsService.getPrintVehiclesFLByCampaign(this.date).subscribe((res: Array<object>) => {
       this.printFL = res;
       this.loading_printFL = false;
    });
  }

  public getTotalPopByCampaign() {
    this.loading_total_open = true;
    this.dashboardSmsService.getTotalPopByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_open = res;
       this.loading_total_open = false;
    });
  }

  public getUsersInTheBusByCampaign() {
    this.loading_user_in_the_bus = true;
    this.dashboardSmsService.getUsersInTheBusByCampaign(this.date).subscribe((res: Array<object>) => {
       this.user_in_the_bus = res[0];
       this.allUserInTheBus = res[1];
       this.loading_user_in_the_bus = false;
    });
  }


  public getTotalPartcipantWeekByCampaign() {
    this.loading_total_participant_week = true;
    this.dashboardSmsService.getTotalParticipantWeekByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_participant_week = res.length;
       this.loading_total_participant_week = false;
    });
  }

  public getTotalSmsReceivedByCampaign() {
    this.loading_total_sms_received = true;
    this.dashboardSmsService.getTotalSmsreceivedByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_sms_received = res;
       this.loading_total_sms_received = false;
    });
  }

  public getAllSurveyCampaigns() {
    this.dashboardSmsService.getAllSurveyCampaigns(this.date).subscribe((res: Array<object>) => {
       this.total_survey = res;
    });
  }

  public onSelected(value) {
    this.selectCampaign = {'survey': value};
    this.dashboardSmsService.selectedCampaign(this.selectCampaign).subscribe((res: Array<object>) => {
      this.selected = true;
      this.firstDate = res[0];
      this.secondDate = res[1];
      this.surveyInfo = res[2];
   });
  }

  public chartActivationNumber() {
    this.dashboardSmsService.ChartActivationNumberOnRapport(this.date).subscribe((res: Array<object>) => {
       this.activationNumber = res;
         //console.log('activation', this.activationNumber);
       this.chart();
    });
  }

  public chartActivationNewNumber() {
    this.dashboardSmsService.ChartActivationNewNumber(this.date).subscribe((res: Array<object>) => {
       this.activationNewNumber = res;
         //console.log('activation', this.activationNumber);
       this.chartNewNumber();
    });
  }

  public chartActivationPop() {
    this.dashboardSmsService.ChartActivationPop(this.date).subscribe((res: Array<object>) => {
      this.activationPop = res[0];
      //this.activationDate = res[1];
       // console.log('activation pop', res);
       this.chart();
    });
  }

  public chartActivationDevice() {
    this.dashboardSmsService.ChartActivationDevice(this.date).subscribe((res: Array<object>) => {
      this.activationDevice = res[0];
      this.activationDate = res[1];
       // console.log('activation device', this.activationDevice);
       this.chart();
    });
  }

  public chartStickedBus() {
    this.dashboardSmsService.ChartStickedBus(this.date).subscribe((res: Array<object>) => {
      this.stickedBus = res;
       // console.log('sticked bus', this.stickedBus);
       this.chart();
    });
  }

  /*chart() {
    const data = {
      chart: {
        caption: 'Activation, Écrans Ouverts et Pop',
        subcaption: '',
        yaxisname: 'Nombre',
        syaxisname: '',
        snumbersuffix: '',
        drawcustomlegendicon: '0',
        showvalues: '0',
        rotatelabels: '1',
        theme: 'fusion',
      },
      categories: [
        {
          category: this.activationDate,
        },
      ],
      dataset: [
        {
          seriesname: 'Activation',
          data: this.activationNumber,
        },
        {
          seriesname: 'Pop Echelle 1/1000',
          data: this.activationPop,
        },
        {
          seriesname: 'écrans ouverts',
          renderas: 'line',
          // parentyaxis: "S",
          data: this.activationDevice,
        },
      ],
    };

  this.width = 900;
  this.height = 400;
  this.type = 'mscombidy2d';
  this.dataFormat = 'json';
  this.dataSource = data;
  }*/

  chart() {
    // console.group('-- entrer Chart--');
    this.chartConfigActivation = {
      width: '950',
      height: '450',
      type: 'column2d',
      dataFormat: 'json',
   };

   this.dataSourceActivation = {
      'chart': {
        'caption': 'Utilisateurs uniques',
    subcaption: '',
    yaxisname: 'Nombre',
    syaxisname: 'Date',
    snumbersuffix: '',
    drawcustomlegendicon: '0',
    showvalues: '0',
    rotatelabels: '1',
    theme: 'fusion',
      },
      'data': this.activationNumber,
    };

  }

  chartNewNumber() {
    // console.group('-- entrer Chart--');
    this.chartConfigNewNumber = {
      width: '950',
      height: '450',
      type: 'column2d',
      dataFormat: 'json',
   };

   this.dataSourceNewNumber = {
      'chart': {
        'caption': 'Nouveaux utilisateurs uniques',
    subcaption: '',
    yaxisname: 'Nombre',
    syaxisname: 'Date',
    snumbersuffix: '',
    drawcustomlegendicon: '0',
    showvalues: '0',
    rotatelabels: '1',
    theme: 'fusion',
      },
      'data': this.activationNewNumber,
    };

  }

  public chartJob() {
        this.dashboardSmsService.ChartJob(this.date).subscribe((res: Array<object>) => {
          this.jobRepartition = res[0];
          this.lineRepartition = res[1];
           // console.log('chart job', res);
           this.getChartJob();
           this.getChartLine();
        });
      }
      getChartJob() {
        // console.group('-- entrer Chart--');
        this.chartConfigJob = {
          width: '450',
          height: '450',
          type: 'pie2d',
          dataFormat: 'json',
       };

       this.dataSourceJob = {
          'chart': {
            'caption': 'Répartition par métier',
            'subCaption': '',
            'numberPrefix': '',
            'showPercentInTooltip': '0',
            'decimals': '1',
            'useDataPlotColorForLabels': '1',
            // Theme
            'theme': 'fusion',

          },
          'data': this.jobRepartition,
        };

      }

      getChartLine() {
        // console.group('-- entrer Chart--');
        this.chartConfigLine = {
          width: '450',
          height: '450',
          type: 'column2d',
          dataFormat: 'json',
       };

       this.dataSourceLine = {
          'chart': {
            'caption': 'Répartition par Ligne',
        subcaption: '',
        yaxisname: 'Nombre',
        syaxisname: 'Lignes',
        snumbersuffix: '',
        drawcustomlegendicon: '0',
        showvalues: '0',
        rotatelabels: '1',
        theme: 'fusion',
          },
          'data': this.lineRepartition,
        };

      }

  // chart gender
  public chartGender() {
    this.dashboardSmsService.ChartGender(this.date).subscribe((res: Array<object>) => {
      this.chartGenderValue = res[0];
      this.chartGenderLegend = res[1];
        // console.log('chart gender', this.chartGenderLegend, this.chartGenderValue);
       this.getChartGender();
    });
  }
  getChartGender() {
    // console.group('-- entrer Chart--');
    this.chartConfigGender = {
      width: '450',
      height: '450',
      type: 'pie2d',
      dataFormat: 'json',
   };

   this.dataSourceGender = {
      'chart': {
        'caption': 'Répartition par genre',
        'subCaption': '',
        'numberPrefix': '',
        'showPercentInTooltip': '0',
        'decimals': '1',
        'useDataPlotColorForLabels': '1',
        // Theme
        'theme': 'fusion',

      },
      'data': this.chartGenderValue,
    };

  }

  public chartAge() {
    this.dashboardSmsService.ChartAge(this.date).subscribe((res: Array<object>) => {
      this.chartAgeValue = res[0];
      // this.chartGenderLegend = res[1];
        //console.log('chart age', this.chartAgeValue);
       this.getChartAge();
    });
  }
  getChartAge() {
    console.group('-- entrer Chart--');
    this.chartConfigAge = {
      width: '400',
      height: '400',
      type: 'bar2d',
      dataFormat: 'json',
   };

   this.dataSourceAge = {
      'chart': {
        caption: 'Répartition par tranche d\'âge',
    yaxisname: '',
    aligncaptionwithcanvas: '0',
    plottooltext: '<b>$dataValue</b> personnes',
    theme: 'fusion',

      },
      'data': this.chartAgeValue,
    };

  }

}
