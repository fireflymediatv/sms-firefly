import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from '@angular/router';
import { IndexRapportComponent } from './index/index.component';
import { FusionChartsModule } from 'angular-fusioncharts';
// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
// Add dependencies to FusionChartsModule
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

export const RapportRoute: Routes = [
  { path: 'rapport', component: IndexRapportComponent },
  // { path: 'participant/add', component: AddParticipantComponent },
  // { path: 'keyword/edit/:id', component: EditKeywordComponent },
  // { path: 'keyword/:id', component: GetKeywordComponent }
];
import { RapportRoutingModule,routedComponents   } from './rapport-routing.module';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    TableModule,
    RapportRoutingModule,
    FusionChartsModule,
  ],
})
export class RapportModule { }
