import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexRapportComponent } from './index/index.component';

const routes: Routes = [
  { path: 'rapport', component: IndexRapportComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RapportRoutingModule { }

export const routedComponents = [
  IndexRapportComponent,
];
