import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbPopoverModule,
} from '@nebular/theme';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CustomerRoutingModule, routedComponents  } from './customer-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from "@angular/router";
import {MatTableModule} from '@angular/material/table';

import { IndexCustomerComponent } from './index/index.component';
//import { AddComponent } from './add/add.component';
import { GetCustomerComponent } from './get/get.component';
//import { EditComponent } from './edit/edit.component';
import { FusionChartsModule } from 'angular-fusioncharts';
// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
// Add dependencies to FusionChartsModule
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

export const campaignRoute: Routes = [
  { path: 'customer', component: IndexCustomerComponent },
  //{ path: 'campaign/add', component: AddComponent },
  //{ path: 'campaign/edit/:id', component: EditComponent },
  { path: 'customer/:id', component: GetCustomerComponent }
];

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    CustomerRoutingModule,
    TableModule,
    NbPopoverModule,
    MatTableModule,
    FusionChartsModule,
    RouterModule.forChild(campaignRoute),
  ],
   declarations: [
    ...routedComponents,
  ],
})
export class CustomerModule { }


