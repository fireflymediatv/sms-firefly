import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CampaignComponent } from './campaign.component';
import { IndexCustomerComponent } from './index/index.component';
//import { AddComponent } from './add/add.component';
import { GetCustomerComponent } from './get/get.component';
//import { EditComponent } from './edit/edit.component';

export const customerRoute: Routes = [{ 
     path: '',
     component: CampaignComponent,
     children: [
          { path: 'customer', component: IndexCustomerComponent },
          //{ path: 'campaign/add', component: AddComponent },
          //{ path: 'campaign/edit/:id', component: EditComponent },
          { path: 'customer/:id', component: GetCustomerComponent },
        ],
      }];

@NgModule({
  imports: [
    RouterModule.forChild(customerRoute),
  ],
  exports: [
    RouterModule,
  ],
})
export class CustomerRoutingModule {
}

export const routedComponents = [
  IndexCustomerComponent,
  //AddComponent,
  GetCustomerComponent,
  //EditComponent
];