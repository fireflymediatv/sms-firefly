import { Component, OnInit,ChangeDetectionStrategy, ViewChild, TemplateRef, } from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { SurveyCampaignService } from '../../../services/surveycampaign.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [SurveyCampaignService],
  //changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexCustomerComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public survey_campaign;
  public data ;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  public popoverTitle: String = 'Cette action est irréversible';
    public popoverMessage: String = 'Êtes-vous sûr de vouloir arrêter cette campagne ?';
    public confirmClicked: Boolean = false;
    public cancelClicked: Boolean = false;
  statuses: NbComponentStatus[] = [ 'primary'];

  constructor(private curveycampaignservice :  SurveyCampaignService,
              private router: Router,
              private route: ActivatedRoute) {
                this.router = router;
               }

  ngOnInit(): void{
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [
      { key: 'name', title: 'Nom' },
      { key: 'count', title: 'Nombre de campagnes' },
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getActiveCustomers();
  }

   // Show All Sms partner
   public getActiveCustomers() {
     var obj;
    this.curveycampaignservice.getActiveCustomers(obj).subscribe((data: Array<object>) => {
       //console.log(' Sms survey campaign', data);
       this.configuration.isLoading =false;
       this.survey_campaign = data;
       this.data = this.survey_campaign;
    }); 
  }

}
