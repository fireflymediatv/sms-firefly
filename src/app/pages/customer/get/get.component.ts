import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { SurveyCampaignService } from '../../../services/surveycampaign.service';
import { DashboardSmsService } from '../../../services/dashboard-sms.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-get',
  templateUrl: './get.component.html',
  styleUrls: ['./get.component.scss'],
  providers : [SurveyCampaignService, DashboardSmsService],
})
export class GetCustomerComponent implements OnInit {
  customerSurvey;
  public configuration: Config;
  public columns: Columns[];
  data;
  date;
  submittedDate = false;
  dateSmsForm: FormGroup;
  color;
  tabData = [];
  diffInDays;
  selected = false;
  firstDate;
  surveyInfo;
  secondDate;
  width;
  height;
  type;
  dataFormat;
  dataSource;
  selectCampaign;
  activationNumber;
  activationDate;
  activationPop;
  activationDevice;
  stickedBus;
  chartConfigGender: Object;
  dataSourceGender: {};
  chartConfigAge: Object;
  dataSourceAge: {};
  chartConfigJob: Object;
  dataSourceJob: {};
  chartConfigLine: Object;
  dataSourceLine: {};
  chartGenderLegend;
  chartGenderValue;
  chartAgeValue;
  jobRepartition;
  lineRepartition;
  @ViewChild('dateTpl', { static: true }) dateTpl: TemplateRef<any>;
  @ViewChild('imgTpl', { static: true }) imgTpl: TemplateRef<any>;
  @ViewChild('nameTpl', { static: true }) nameTpl: TemplateRef<any>;
  @ViewChild('allUserTpl', { static: true }) allUserTpl: TemplateRef<any>;
  @ViewChild('uniqueUserTpl', { static: true }) uniqueUserTpl: TemplateRef<any>;

  @ViewChild('date') firstName: ElementRef<any>;
  @ViewChild('img') lastName: ElementRef<any>;
  @ViewChild('name') phone_number: ElementRef<any>;
  @ViewChild('allUser') age: ElementRef<any>;
  @ViewChild('uniqueUser') gender: ElementRef<any>;
  constructor(
    private surveycampaignservice:  SurveyCampaignService,
    private dashboardSmsService:  DashboardSmsService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) {
    this.router = router;
    this.dateSmsForm = this.fb.group({
      campaign : ['', Validators.required],
     });
  }

  ngOnInit(): void {
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [
      { key: 'date', title: 'Date', cellTemplate: this.dateTpl },
      { key: 'img', title: 'Image', cellTemplate: this.imgTpl },
      { key: 'name', title: 'Nom', cellTemplate: this.nameTpl },
      { key: 'allUser', title: 'Nombre de campagnes', cellTemplate: this.allUserTpl },
      { key: 'uniqueUser', title: 'Nom', cellTemplate: this.uniqueUserTpl },
      // { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getSurveyByCustomerId();
  }

  public getSurveyByCustomerId() {
    this.route.params.subscribe(params => {
        this.surveycampaignservice.get(params['id']).subscribe(res => {
          this.customerSurvey = res;
          this.data = this.customerSurvey;
          this.configuration.isLoading = false;
        // console.log("Rapport campaign",res);
        });
    });
}

// convenience getter for easy access to form fields
get fDateProbe() {
  return this.dateSmsForm.controls;
}

onSubmitDateInterval() {
  this.submittedDate = true;
  if (this.dateSmsForm.invalid) {
    console.log('error date');
    return this.dateSmsForm.controls;
  }
  this.date = {
      'campaign' : this.fDateProbe.campaign.value,
  };
  // console.log('submit campaign', this.date);
  this.chartJob();
 this.chartGender();
 this.chartAge();
 this.chartActivationPop();
 this.chartActivationNumber();
 this.chartActivationDevice();
 this.chartStickedBus();
}

public onSelected(value) {
  this.selectCampaign = {'survey': value};
  this.dashboardSmsService.selectedCampaign(this.selectCampaign).subscribe((res: Array<object>) => {
    this.selected = true;
    this.firstDate = res[0];
    this.secondDate = res[1];
    this.surveyInfo = res[2];
 });
}

public chartActivationNumber() {
  this.dashboardSmsService.ChartActivationNumber(this.date).subscribe((res: Array<object>) => {
     this.activationNumber = res[1];
      // console.log('activation', this.activationDate);
     this.chart();
  });
}

public chartActivationPop() {
  this.dashboardSmsService.ChartActivationPop(this.date).subscribe((res: Array<object>) => {
    this.activationPop = res[0];
    this.activationDate = res[1];
     // console.log('activation pop', this.activationPop);
     this.chart();
  });
}

public chartActivationDevice() {
  this.dashboardSmsService.ChartActivationDevice(this.date).subscribe((res: Array<object>) => {
    this.activationDevice = res;
     // console.log('activation device', this.activationDevice);
     this.chart();
  });
}

public chartStickedBus() {
  this.dashboardSmsService.ChartStickedBus(this.date).subscribe((res: Array<object>) => {
    this.stickedBus = res;
     // console.log('sticked bus', this.stickedBus);
     this.chart();
  });
}

chart() {
  const data = {
    chart: {
      caption: 'Activation, Écrans Ouverts, Bus Stickés et Pop',
      subcaption: '',
      yaxisname: 'Nombre',
      syaxisname: '',
      snumbersuffix: '',
      drawcustomlegendicon: '0',
      showvalues: '0',
      rotatelabels: '1',
      theme: 'fusion',
    },
    categories: [
      {
        category: this.activationDate,
      },
    ],
    dataset: [
      {
        seriesname: 'Activation',
        data: this.activationNumber,
      },
      {
        seriesname: 'Pop Echelle 1/1000',
        data: this.activationPop,
      },
      {
        seriesname: 'écrans ouverts',
        renderas: 'line',
        // parentyaxis: "S",
        data: this.activationDevice,
      },
      {
        seriesname: 'Bus stickés',
        renderas: 'line',
        // parentyaxis: "S",
        data: this.stickedBus,
      },
    ],
  };

this.width = 900;
this.height = 400;
this.type = 'mscombidy2d';
this.dataFormat = 'json';
this.dataSource = data;
}

public chartJob() {
      this.dashboardSmsService.ChartJob(this.date).subscribe((res: Array<object>) => {
        this.jobRepartition = res[0];
        this.lineRepartition = res[1];
         // console.log('chart job', res);
         this.getChartJob();
         this.getChartLine();
      });
    }
    getChartJob() {
      // console.group('-- entrer Chart--');
      this.chartConfigJob = {
        width: '450',
        height: '450',
        type: 'pie2d',
        dataFormat: 'json',
     };

     this.dataSourceJob = {
        'chart': {
          'caption': 'Répartition par métier',
          'subCaption': '',
          'numberPrefix': '',
          'showPercentInTooltip': '0',
          'decimals': '1',
          'useDataPlotColorForLabels': '1',
          // Theme
          'theme': 'fusion',

        },
        'data': this.jobRepartition,
      };

    }

    getChartLine() {
      // console.group('-- entrer Chart--');
      this.chartConfigLine = {
        width: '450',
        height: '450',
        type: 'column2d',
        dataFormat: 'json',
     };

     this.dataSourceLine = {
        'chart': {
          'caption': 'Répartition par Ligne',
      subcaption: '',
      yaxisname: 'Nombre',
      syaxisname: 'Lignes',
      snumbersuffix: '',
      drawcustomlegendicon: '0',
      showvalues: '0',
      rotatelabels: '1',
      theme: 'fusion',
        },
        'data': this.lineRepartition,
      };

    }

// chart gender
public chartGender() {
  this.dashboardSmsService.ChartGender(this.date).subscribe((res: Array<object>) => {
    this.chartGenderValue = res[0];
    this.chartGenderLegend = res[1];
      // console.log('chart gender', this.chartGenderLegend, this.chartGenderValue);
     this.getChartGender();
  });
}
getChartGender() {
  // console.group('-- entrer Chart--');
  this.chartConfigGender = {
    width: '450',
    height: '450',
    type: 'pie2d',
    dataFormat: 'json',
 };

 this.dataSourceGender = {
    'chart': {
      'caption': 'Répartition par genre',
      'subCaption': '',
      'numberPrefix': '',
      'showPercentInTooltip': '0',
      'decimals': '1',
      'useDataPlotColorForLabels': '1',
      // Theme
      'theme': 'fusion',

    },
    'data': this.chartGenderValue,
  };

}

public chartAge() {
  this.dashboardSmsService.ChartAge(this.date).subscribe((res: Array<object>) => {
    this.chartAgeValue = res[0];
    // this.chartGenderLegend = res[1];
      console.log('chart age', this.chartAgeValue);
     this.getChartAge();
  });
}
getChartAge() {
  console.group('-- entrer Chart--');
  this.chartConfigAge = {
    width: '400',
    height: '400',
    type: 'bar2d',
    dataFormat: 'json',
 };

 this.dataSourceAge = {
    'chart': {
      caption: 'Répartition par tranche d\'âge',
  yaxisname: '',
  aligncaptionwithcanvas: '0',
  plottooltext: '<b>$dataValue</b> personnes',
  theme: 'fusion',

    },
    'data': this.chartAgeValue,
  };

}

  public onClickButton(value, value1) {
    this.tabData[0] = value;
    this.tabData[1] = value1;
    // console.log('value', this.tabData);
     this.surveycampaignservice.boostOrRemakeCampaign(this.tabData).subscribe((data: Array<object>) => {
      this.getSurveyByCustomerId();
     });
  }

}
