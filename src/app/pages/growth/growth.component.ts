import {TemplateRef, ViewChild, Component, OnInit, OnDestroy, OnChanges, DoCheck, ChangeDetectionStrategy } from '@angular/core';
import { GrowthService } from '../../services/growth.service';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NbDateService } from '@nebular/theme';
import { NbThemeService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { SurveyCampaignService} from '../../services/surveycampaign.service';
import { ExportToCSV } from '@molteni/export-csv';
import { Router } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

const ELEMENT_DATA: any[] = [];
const userRole = 'user';

@Component({
  selector: 'ngx-growth',
  templateUrl: './growth.component.html',
  styleUrls: ['./growth.component.scss'],
  providers : [GrowthService, SurveyCampaignService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrowthComponent implements OnInit, OnDestroy {
      options: any = {};
      public configuration: Config;
      public columns: Columns[];
      themeSubscription: any;
      route;
      data;
      data_sms;
      growth;
      growths;
      phoneRepartition;
      openRepartition;
      deployedRepartition;
      dateRepartition;
      growthChart;
      growthChartByperiod;
      growthChartByMonth;
      growth_body = [];
      submittedGrowth;
      phoneSevenDays;
      openSevendays;
      submitChart = false;
      surveycampaigns;
      dateSmsForm: FormGroup;
      growthForm: FormGroup;
      chartConfigGrowth: Object;
      dataSourceGrowth = {};
      chartConfigGrowthByMonth: Object;
      chartConfigGrowthOpen: Object;
      chartConfigGrowthDeployment: Object;
      dataSourceGrowthDeployment: {};
      dataSourceGrowthOpen: {};
      dataSourceGrowthByMonth: {};
      chartDatas = [];
      tabBaseCount;
      chartConfigGrowthByPeriod: Object;
      chartConfigNewPhone: Object;
      dataSourceNewPhone: {};
      dataSourceGrowthByPeriod: {};
      chartDataByPeriod = [];
      chartOpen = [];
      defaultOpen;
      chartNewPhone;
      tabDateSevenDays;
      dataSourceSevenDays: {};
      width;
      height;
      type;
      dataFormat;
      dataSource;
      dataSourceDeployed;
      currentUser;
      static parameters = [GrowthService, SurveyCampaignService, FormBuilder, NbDateService];

      constructor(  private growthservice: GrowthService,
                    private surveycampaignservice: SurveyCampaignService,
                    private fb: FormBuilder,
                    private dateService: NbDateService<Date>,
                    private datePipe: DatePipe,
                    private theme: NbThemeService,
                    private router: Router) {

         this.growth_body = [{_id : 0, name: 'Jour'},
                             {_id : 1, name: 'Semaine'},
                             {_id : 2, name: 'Mois'},
                            // {_id : 3, name: 'Trimestre'},
                             {_id : 4, name: 'Année'}];

         this.growthForm = this.fb.group({
          period : ['', Validators.required],
          date : ['', Validators.required],
         });
      }

        // convenience getter for easy access to form fields
    get fDateProbe() {
      return this.dateSmsForm.controls;
    }

    // convenience getter for easy access to form fields
    get fGrowth() {
      return this.growthForm.controls;
    }

    ngOnInit(): void {
      this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    var val, val1, val2;
     // this.configuration.isLoading = true;
     this.configuration = { ...DefaultConfig };
     this.configuration.searchEnabled = true;
       this.columns = [
         { key: '_id.date', title: 'Période' },
         { key: 'countBase', title: 'Nombre' },
         { key: 'growth_rate_month', title: 'taux de croissance' },
         { key: 'month_difference', title: 'Différence ' },
         { key: 'open', title: 'nouveaux numéros/écrans ouverts' },
         { key: 'deployment', title: 'nouveaux numéros/écrans déployés' },
       ];
       this.getGrowth();
       this.getGrowthChart();
       this.updateMyChartData();
       this.phoneAndOpenRepartition(val, val1, val2);
    }

    ngOnDestroy(): void {
      console.log('--OnDestrooy---');
      if (this.themeSubscription)
        this.themeSubscription.unsubscribe();
     //  this.router.navigateByUrl('/auth/login');
    }

    // Show All Growth
    public getGrowth() {
      this.growthservice.getGrowth().subscribe((data: Array<object>) => {
        this.growths = data[0];
        this.openRepartition = data[1];
        this.deployedRepartition = data[2];
        this.chartNewPhone = data[3];
        this.chartPhoneAndOpenRepartition();
        this.chartPhoneAndDeployedRepartition();
        this.chartNewPhoneNumber()
      });
    }

    public phoneAndOpenRepartition(obj_id, startDate, endDate) {
      // tslint:disable-next-line: max-line-length
      this.growthservice.getPhoneAndOpenRepartition({'period': obj_id, 'startDate': startDate, 'endDate': endDate}).subscribe((data: Array<object>) => {
        this.dateRepartition = data[0];
        this.phoneRepartition = data[2];
        //this.openRepartition = data[2];
        //this.deployedRepartition = data[3];
        this.tabBaseCount = data[4];
        this.chartPhoneAndOpenRepartition();
        this.chartPhoneAndDeployedRepartition();
        this.chartSevenDaysRepartition();
        // console.log('growth', this.growth);
      });
    }

    public getGrowthChart() {
      this.growthservice.getGrowthChart().subscribe((data: Array<object>) => {
        this.growthChart = data;
        for (let i = this.growthChart.length; i--; i >= 0) {
          if (this.growthChart.length > 0) {
            // tslint:disable-next-line: max-line-length
            this.chartDatas.push({label: this.growthChart[i].format_date.substring(0, 12), value: this.growthChart[i].growth_rate_month});
          }
        }
        // console.log(this.chartDatas);
        this.updateMyChartData();
      });
    }

  updateMyChartData() {
    // fusion chart
    this.chartConfigGrowth = {
     width: '500',
     height: '400',
     type: 'line',
     dataFormat: 'json',
  };

  this.dataSourceGrowth = {
     'chart': {
      caption: 'Taux de croissance par jour',
      subcaption: '',
      yaxisname: 'Evolution',
      xaxisname: 'Date',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.chartDatas,
   };

  }

  updateMyChartDataByPeriod() {
    // fusion chart
    this.chartConfigGrowthByPeriod = {
     width: '500',
     height: '400',
     type: 'line',
     dataFormat: 'json',
  };

  this.dataSourceGrowthByPeriod = {
     'chart': {
      caption: 'Taux de croissance par periode',
      subcaption: '',
      yaxisname: 'Evolution',
      xaxisname: 'Date',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.chartDataByPeriod,
   };

  }

     // Submit Campaign
  onSubmitGrowth() {
      this.submittedGrowth = true;
      this.submitChart = true;
      if (this.growthForm.invalid) {
        console.log('error survey form');
        return this.growthForm.controls;
      }
      this.chartDataByPeriod = [];
      console.log(' Id growth ', this.fGrowth.period.value, this.fGrowth.date.value.start, this.fGrowth.date.value.end);
      //this.getPeriod(this.fGrowth.period.value, this.fGrowth.date.value.start, this.fGrowth.date.value.end);
      this.getGrowthChartByPeriod(this.fGrowth.period.value, this.datePipe.transform(this.fGrowth.date.value.start), this.datePipe.transform(this.fGrowth.date.value.end));
      this.phoneAndOpenRepartition(this.fGrowth.period.value, this.datePipe.transform(this.fGrowth.date.value.start), this.datePipe.transform(this.fGrowth.date.value.end));
  }

   // Show All Sms
   public getPeriod(obj_id, startDate, endDate) {
    console.log('--get Period  value Id :', obj_id);
    // tslint:disable-next-line: max-line-length
    this.growthservice.getGrowthPeriod({'period': obj_id, 'startDate': startDate, 'endDate': endDate}).subscribe((data: Array<object>) => {
       //this.growths = data; // update dat;a list
       //console.log('day G',this.growths);
    });

  }

  public getGrowthChartByPeriod(obj_id, startDate, endDate) {
    // tslint:disable-next-line: max-line-length
    this.growthservice.getGrowthPeriod({'period': obj_id, 'startDate': startDate, 'endDate': endDate}).subscribe((data: Array<object>) => {
      this.growthChartByperiod = data[0];
      this.growths = data[0];
      for (let i = 0; i < this.growthChartByperiod.length; i++) {
        if (this.growthChartByperiod.length > 0) {
          this.chartDataByPeriod.push({label: this.growthChartByperiod[i]._id.date, value: this.growthChartByperiod[i].growth_rate_month});
        }
      }
      this.openRepartition = data[1];
      this.deployedRepartition = data[2];
      this.chartNewPhone = data[3];
      this.chartPhoneAndOpenRepartition();
      this.chartPhoneAndDeployedRepartition();
      this.updateMyChartDataByPeriod();
      this.chartSevenDaysRepartition();
      this.chartNewPhoneNumber();
    });
  }



  chartPhoneAndOpenRepartition() {
    // fusion chart
    this.chartConfigGrowthOpen = {
     width: '500',
     height: '400',
     type: 'line',
     dataFormat: 'json',
  };
  this.dataSourceGrowthOpen = {
     'chart': {
      caption: 'Répartition entre nouveaux numéros et écrans ouverts',
      subcaption: '',
      yaxisname: 'Nombre',
      xaxisname: 'Date',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.openRepartition,
   };
  }

  chartPhoneAndDeployedRepartition() {
    // fusion chart
    this.chartConfigGrowthDeployment = {
     width: '500',
     height: '400',
     type: 'line',
     dataFormat: 'json',
  };
  this.dataSourceGrowthDeployment = {
     'chart': {
      caption: 'Répartition entre nouveaux numéros et écrans déployés',
      subcaption: '',
      yaxisname: 'Nombre',
      xaxisname: 'Date',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.deployedRepartition,
   };
  }


  chartNewPhoneNumber() {
    // fusion chart
    this.chartConfigNewPhone = {
     width: '500',
     height: '400',
     type: 'column2d',
     dataFormat: 'json',
  };
  this.dataSourceNewPhone = {
     'chart': {
      caption: 'Répartition des nouveaux numéros par période',
      subcaption: '',
      yaxisname: 'Nombre',
      xaxisname: 'Date',
      forceaxislimits: '1',
      pixelsperpoint: '0',
      pixelsperlabel: '30',
      compactdatamode: '1',
      theme: 'fusion',
     },
     'data': this.chartNewPhone,
   };
  }

  chartSevenDaysRepartition() {
    const data = {
      chart: {
        caption: 'Base de donnés users',
        subcaption: 'Nouveaux numéros/écrans ouverts',
        yaxisname: 'Nombre',
        syaxisname: '',
        snumbersuffix: '',
        drawcustomlegendicon: '0',
        showvalues: '0',
        rotatelabels: '1',
        theme: 'fusion',
      },
      categories: [
        {
          category: this.dateRepartition,
        },
      ],
      dataset: [
        {
          seriesname: 'Nouveaux numéros: Échelle 1/100',
          renderas: 'column2d',
          data: this.tabBaseCount,
        },
        {
          seriesname: 'Écrans ouverts',
          renderas: 'line',
          data: this.phoneRepartition,
        },
      ],
    };

  this.width = 500;
  this.height = 400;
  this.type = 'mscombidy2d';
  this.dataFormat = 'json';
  this.dataSourceSevenDays = data;
  }

    }

