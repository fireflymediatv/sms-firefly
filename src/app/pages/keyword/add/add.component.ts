import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { KeywordService } from '../../../services/keyword.service';
import { KeywordTypeService} from '../../../services/keywordtype.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers : [KeywordService, KeywordTypeService],
})
export class AddKeywordComponent implements OnInit {
  keywordForm: FormGroup;
  submittedKeyword = false;
  keywords;
  typekeywords;

  constructor(private fb: FormBuilder,
              private keywordservice: KeywordService,
              private keywordtypeservice: KeywordTypeService,
              private router: Router,
              private route: ActivatedRoute ) {
    this.keywordForm = this.fb.group({
      name : ['', Validators.required],
      short_code : ['', Validators.required],
      forecast_volume : ['', Validators.required],
      keyword_type_id : ['', Validators.required],
     });

  }

  // convenience getter for easy access to form fields
  get fKeyword() {
    return this.keywordForm.controls;
  }

     // Submit form
   onSubmitKeyword() {
        this.submittedKeyword = true;
        console.log('form keyword', this.fKeyword);
        if (this.keywordForm.invalid) {
          console.log('error keyword form');
          return this.keywordForm.controls; ;
        }
        let form_tkeyword = this.fKeyword.name.parent.value;
        console.log('final value', form_tkeyword );
        this.keywordservice.createKeyword(form_tkeyword).subscribe((data: Array<object>) => {
          this.fKeyword.name.reset();
          this.fKeyword.keyword_type_id.reset();
          console.log(' successfully create campaign');
          this.router.navigate(['pages/keyword']);
       });
    }

  ngOnInit() {
    this.getKeywords();
    this.getTypeKeywords();
  }

  // Show All keyword
  public getKeywords() {
    this.keywordservice.getKeywords().subscribe((data: Array<object>) => {
       console.log(' Sms keywords services', data);
       this.keywords = data;
    });
  }

    // Show All type keyword
    public getTypeKeywords() {
      this.keywordtypeservice.getTypeKeywords().subscribe((data: Array<object>) => {
         console.log(' Sms types keywords services', data);
         this.typekeywords = data;
      });
    }

}
