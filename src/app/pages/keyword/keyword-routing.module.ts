import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexKeywordComponent } from './index/index.component';
import { AddKeywordComponent } from './add/add.component';
import { GetKeywordComponent } from './get/get.component';
import { EditKeywordComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'keyword', component: IndexKeywordComponent },
  { path: 'keyword/add', component: AddKeywordComponent },
  { path: 'keyword/edit/:id', component: EditKeywordComponent },
  { path: 'keyword/:id', component: GetKeywordComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KeywordRoutingModule { }

export const routedComponents = [
  IndexKeywordComponent,
  AddKeywordComponent ,
  EditKeywordComponent,
  GetKeywordComponent,
];
