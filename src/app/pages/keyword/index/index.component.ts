import {TemplateRef, ViewChild, Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { KeywordService } from '../../../services/keyword.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [KeywordService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexKeywordComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public keywords;
  public data ;
  currentUser;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;


  constructor(private keywordservice:  KeywordService,
              private router: Router,
              private route: ActivatedRoute) {
                this.router = router;
               }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [
      { key: 'name', title: 'Nom' },
      { key: 'short_code', title: 'Code court' },
      { key: 'forecast_volume', title: 'Volume prévu' },
      { key: 'keyword_type_id.name', title: 'Type mot clé' },
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getKeywords();
  }

   // Show All Sms partner
   public getKeywords() {
    this.keywordservice.getKeywords().subscribe((data: Array<object>) => {
       console.log(' Sms keyword campaign', data);
       this.configuration.isLoading = false;
       this.keywords = data;
       this.data = this.keywords;
    });
  }

  edit(rowIndex: number): void {
    this.selectRow = rowIndex;
  }

  update(rowIndex: number): void {
    this.selectRow = rowIndex;
    console.log('---update--', this.selectRow);
    this.data.map((obj, index) => {
        if (index === this.selectRow) {
          console.log('on est ici', obj);
        }
      });
    this.selectRow = -1;
  }

  delete(rowIndex: number): void {
    this.selectRow = rowIndex;
    console.log('---delete--', this.selectRow);
    this.data.map((obj, index) => {
        if (index === this.selectRow) {
          console.log('on est au delete', obj);
          this.keywordservice.remove(obj).subscribe(res => {
            console.log('keyword deleted');
            this.getKeywords();
          });
        }
      });
    this.selectRow = -1;
  }

}
