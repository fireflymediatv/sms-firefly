import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from '@angular/router';

import { IndexKeywordComponent } from './index/index.component';
import { AddKeywordComponent } from './add/add.component';
import { GetKeywordComponent } from './get/get.component';
import { EditKeywordComponent } from './edit/edit.component';

export const keywordRoute: Routes = [
  { path: 'keyword', component: IndexKeywordComponent },
  { path: 'keyword/add', component: AddKeywordComponent },
  { path: 'keyword/edit/:id', component: EditKeywordComponent },
  { path: 'keyword/:id', component: GetKeywordComponent },
];
import { KeywordRoutingModule, routedComponents   } from './keyword-routing.module';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    KeywordRoutingModule,
    TableModule,
    KeywordRoutingModule,
    RouterModule.forChild(keywordRoute),
  ],
})
export class KeywordModule { }
