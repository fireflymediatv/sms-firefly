import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { NbMenuModule } from '@nebular/theme';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { CampaignModule } from './campaign/campaign.module';
import { KeywordModule } from './keyword/keyword.module';
import { ResponseModule } from './response/response.module';
import { MessageModule } from './message/message.module';
import { SmsreceiveModule } from './smsreceive/smsreceive.module';
import { AudienceModule } from './audience/audience.module';
import { GrowthModule } from './growth/growth.module';
import { ParticipantModule } from './participant/participant.module';
import { DashboardSmsModule } from './dashboard-sms/dashboard-sms.module';
import { DashboardSuiviCampaignModule } from './suivi-campaign/suivi-campaign.module';
import { RapportModule } from './rapport/rapport.module';
import { UssdreceiveModule } from './ussdreceive/ussdreceive.module';
import { UploadParticipantModule } from './uploadparticipant/uploadparticipant.module';
import { CustomerModule } from './customer/customer.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NbMomentDateModule } from '@nebular/moment';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { fr } from 'date-fns/locale';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgApexchartsModule} from 'ng-apexcharts';
import { MatFormFieldModule} from '@angular/material/form-field';
import { TableModule } from 'ngx-easy-table';

@NgModule({
  imports: [
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,

    NgxEchartsModule,
    NgApexchartsModule,
    ClipboardModule,
    FormsModule,
    ReactiveFormsModule,
    CdkStepperModule,
    PortalModule,
    ScrollingModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    PagesRoutingModule,
    ThemeModule,
    MatTabsModule,
    MatToolbarModule,
    MatTreeModule,
    MatTooltipModule,
    MatInputModule,
    NbMenuModule,
    MatTableModule,
    SmsreceiveModule,
    AudienceModule,
    GrowthModule,
    ParticipantModule,
    DashboardSmsModule,
    DashboardSuiviCampaignModule,
    RapportModule,
    UploadParticipantModule,
    DashboardModule,
    CampaignModule,
    KeywordModule,
    ResponseModule,
    UssdreceiveModule,
    MessageModule,
    ECommerceModule,
    MiscellaneousModule,
    NbDateFnsDateModule,
    NbMomentDateModule,
    BsDatepickerModule,
    MatFormFieldModule,
    TableModule,
    CustomerModule,
    NbDateFnsDateModule.forRoot({
      parseOptions: { locale: fr },
      formatOptions: { locale: fr },
    }),
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
