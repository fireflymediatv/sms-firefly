import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { ParticipantService } from '../../../services/participant.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus, NbThemeService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [ParticipantService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexUploadParticipantComponent implements OnInit {
  public configuration: Config;
  themeSubscription: any;
  currentUser;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;
  fileToUpload;
  submittedCsv = false;
  csvForm: FormGroup;
  loading_submit = false;


  constructor(private participantService:  ParticipantService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private theme: NbThemeService,
              private cdr: ChangeDetectorRef,
              private spinner: NgxSpinnerService) {
                this.router = router;
                this.csvForm = this.fb.group({
                  csvFile : ['', Validators.required],
                 });
               }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
  }

  get fCsvFile() {
    return this.csvForm.controls;
  }
  successAlertNotification(){
    Swal.fire('Opération réussie', 'fichier csv importé avec succès!', 'success')
  }
  uploadCsvFile() {
    this.submittedCsv = true;
    if (this.csvForm.invalid) {
      console.log('error file');
      return this.csvForm.controls;
    }
    /** spinner starts on init */
    this.spinner.show();
    this.loading_submit = true;
    //console.log('uploaded file', this.fileToUpload[0]["activationCampaign\r"])
          this.participantService.uploadCsvFile(this.fileToUpload).subscribe((res) => {
            this.loading_submit = false;
            this.spinner.hide();
            this.successAlertNotification();
          });
          this.loading_submit = false;
  }
  changeListener(files: FileList) {
    // console.log('files', files);
    if (files && files.length > 0) {
       const file: File = files.item(0);
         // File reader method
         const reader: FileReader = new FileReader();
         reader.readAsText(file);
         reader.onload = (e) => {
          const csv: any = reader.result;
          this.csvJSON(csv);
          let allTextLines = [];
          allTextLines = csv.split(/\r|\n|\r/);
      };
    }
  }

   csvJSON(csv) {

    const lines = csv.split('\n');

    const result = [];

    const headers = lines[0].split(',');
    // console.log('files', headers);
    for (let i = 1; i < lines.length; i++) {

      const obj = {};
      const currentline = lines[i].split(',');

      for (let j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }

      result.push(obj);

    }
    this.fileToUpload = result;
    // console.log('files', result[0]);
    // return result; //JavaScript object
    return JSON.stringify(result); // JSON
  }



}
