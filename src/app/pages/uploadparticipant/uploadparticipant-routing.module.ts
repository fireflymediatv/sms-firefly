import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexUploadParticipantComponent } from './index/index.component';

const routes: Routes = [
  { path: 'rapport', component: IndexUploadParticipantComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadParticipantRoutingModule { }

export const routedComponents = [
  IndexUploadParticipantComponent,
];
