import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { DashboardSmsService } from '../../../services/dashboard-sms.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [DashboardSmsService],
  //changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexSuiviComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public probe: any = 0;
  public data;
  dateSmsForm: FormGroup;
  print;
  printStickers;
  printFL;
  user_in_the_bus: any = 0;
  total_open: any = 0;
  total_participant;
  total_participant_week: any = 0;
  total_sms_received:any = 0;
  sticker_user: any = 0;
  print_user: any = 0;
  public estimatedResult;
  public avgProbe;
  loading_total_participant = false;
  loading_total_participant_week = false;
  loading_print_user = false;
  loading_total_open = false;
  loading_user_in_the_bus = false;
  loading_printFL = false;
  loading_printStickers = false;
  loading_print = false;
  loading_data = false;
  loading_probe = false;
  loading_total_sms_received = false;
  loading_total_bus_unique = false;
  total_bus_unique;
  total_survey;
  diffInDays;
  selected = false;
  firstDate;
  surveyInfo;
  secondDate;
  hideDataOnSubmit = false;
  loading_sticker_user = false;
  submittedDate = false;
   date;
   allUserInTheBus;
   selectCampaign;
  editRow: number;
   spaces = price => String(price)
  .replace(
    /(?!^)(?=(?:\d{3})+$)/g,
    ' '
  );
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;


  constructor(private dashboardSmsService :  DashboardSmsService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private cdr: ChangeDetectorRef) {
                this.router = router;
                this.dateSmsForm = this.fb.group({
                  campaign : ['', Validators.required],
                 });
               }

  ngOnInit(): void{
    this.getAllSurveyCampaigns();
  }

  // convenience getter for easy access to form fields
  get fDateProbe() {
    return this.dateSmsForm.controls;
  }

  onSubmitDateInterval() {
          this.submittedDate = true;
          if (this.dateSmsForm.invalid) {
            console.log('error date');
            return this.dateSmsForm.controls;
          }
          this.date = {
              'campaign' : this.fDateProbe.campaign.value,
          };

         this.getTotalProbeByCampaign();
         this.getTotalBusUniqueByCampaign();
         this.getOpenDeviceByCampaign();
         this.getTotalPopByCampaign();
         this.getPrintVehicleByCampaign();
         this.getPrintVehiclesWithStickerByCampaign();
         this.getPrintVehiclesWithFLByCampaign();
         this.getTotalPartcipantWeekByCampaign();
         this.getTotalSmsReceivedByCampaign();
         this.getUsersInTheBusByCampaign();
  }


  public getTotalProbeByCampaign() {
    this.loading_probe = true;
    this.dashboardSmsService.getTotalProbeByCampaign(this.date).subscribe((res: Array<object>) => {
       this.probe = res[0];
       this.avgProbe = res[1];
       this.loading_probe = false;
       //console.log('lenght', this.estimatedResult);
    });
  }

  public getTotalBusUniqueByCampaign() {
    this.loading_total_bus_unique= true;
    this.dashboardSmsService.getTotalUniqueBusByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_bus_unique = res[0];
       this.estimatedResult = res[1];
       this.loading_total_bus_unique = false;
    });
  }
   // Show All Sms partner
   public getOpenDeviceByCampaign() {
     this.loading_data = true;
    this.dashboardSmsService.getOpendeviceByCampaign(this.date).subscribe((data: Array<object>) => {
       this.data = data;
       this.loading_data = false;
    });
  }

  public getPrintVehicleByCampaign() {
    this.loading_print = true;
    this.loading_print_user = true;
    this.dashboardSmsService.getPrintVehicleByCampaign(this.date).subscribe((res: Array<object>) => {
       this.print = res[0];
       this.print_user = res[1];
       this.loading_print_user = false;
       this.loading_print = false;
    });
  }

  public getPrintVehiclesWithStickerByCampaign() {
    this.loading_printStickers = true;
    this.loading_sticker_user = true;
    this.dashboardSmsService.getPrintVehiclesWithStickerByCampaign(this.date).subscribe((res: Array<object>) => {
       this.printStickers = res[0];
       this.sticker_user = res[1];
       this.loading_sticker_user = false;
       this.loading_printStickers = false
    });
  }

  public getPrintVehiclesWithFLByCampaign() {
    this.loading_printFL = true;
    this.dashboardSmsService.getPrintVehiclesFLByCampaign(this.date).subscribe((res: Array<object>) => {
       this.printFL = res;
       this.loading_printFL = false;
    });
  }

  public getTotalPopByCampaign() {
    this.loading_total_open = true;
    this.dashboardSmsService.getTotalPopByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_open = res;
       this.loading_total_open = false;
    });
  }

  public getUsersInTheBusByCampaign() {
    this.loading_user_in_the_bus = true;
    this.dashboardSmsService.getUsersInTheBusByCampaign(this.date).subscribe((res: Array<object>) => {
       this.user_in_the_bus = res[0];
       this.allUserInTheBus = res[1];
       this.loading_user_in_the_bus = false;

    });
  }


  public getTotalPartcipantWeekByCampaign() {
    this.loading_total_participant_week = true;
    this.dashboardSmsService.getTotalParticipantWeekByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_participant_week = res.length;
       this.loading_total_participant_week = false;
    });
  }

  public getTotalSmsReceivedByCampaign() {
    this.loading_total_sms_received = true;
    this.dashboardSmsService.getTotalSmsreceivedByCampaign(this.date).subscribe((res: Array<object>) => {
       this.total_sms_received = res;
       this.loading_total_sms_received = false;
    });
  }

  public getAllSurveyCampaigns() {
    this.dashboardSmsService.getAllSurveyCampaigns(this.date).subscribe((res: Array<object>) => {
       this.total_survey = res;
    });
  }

  public onSelected(value) {
    this.selectCampaign = {'survey': value};
    this.dashboardSmsService.selectedCampaign(this.selectCampaign).subscribe((res: Array<object>) => {
      this.selected = true;
      this.firstDate = res[0];
      this.secondDate = res[1];
      this.surveyInfo = res[2];
   });
  }

}
