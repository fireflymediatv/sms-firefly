import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexSuiviComponent } from './index/index.component';
//import { AddParticipantComponent } from './add/add.component';
//import { GetKeywordComponent } from './get/get.component';
//import { EditKeywordComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'suivi-campaign', component: IndexSuiviComponent },
  //{ path: 'participant/add', component: AddParticipantComponent },
  //{ path: 'keyword/edit/:id', component: EditKeywordComponent },
  //{ path: 'keyword/:id', component: GetKeywordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardSuiviCampaignRoutingModule { }

export const routedComponents = [
  IndexSuiviComponent,
  //AddParticipantComponent ,
  //EditKeywordComponent,
  //GetKeywordComponent
];
