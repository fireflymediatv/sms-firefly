import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from "@angular/router";

import { IndexSuiviComponent } from './index/index.component';
//import { AddParticipantComponent } from './add/add.component';
//import { GetKeywordComponent } from './get/get.component';
//import { EditKeywordComponent } from './edit/edit.component';

export const dashboardRoute: Routes = [
  { path: 'suivi-campaign', component: IndexSuiviComponent },
  //{ path: 'participant/add', component: AddParticipantComponent },
  //{ path: 'keyword/edit/:id', component: EditKeywordComponent },
  //{ path: 'keyword/:id', component: GetKeywordComponent }
];
import { DashboardSuiviCampaignRoutingModule,routedComponents   } from './suivi-campaign-routing.module';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    TableModule,
    DashboardSuiviCampaignRoutingModule,
  ]
})
export class DashboardSuiviCampaignModule { }
