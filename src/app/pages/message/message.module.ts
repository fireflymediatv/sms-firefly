import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from '@angular/router';

import { MessageRoutingModule, routedComponents   } from './message-routing.module';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

@NgModule({
  declarations: [...routedComponents],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    TableModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    MessageRoutingModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger',
           cancelButtonType: 'primary',
           confirmText: 'Supprimer',
           cancelText: 'Annuler' // set defaults here
    }),
  ],
})
export class MessageModule { }
