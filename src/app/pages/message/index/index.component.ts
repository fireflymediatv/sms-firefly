import {TemplateRef, ViewChild, Component, OnInit, ChangeDetectionStrategy, ElementRef, ɵConsole } from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { MessageService } from '../../../services/message.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
const userRole = 'user';
@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  providers : [MessageService],
})
export class IndexMessageComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public messages;
  messageType;
  surveyCampaign;
  responseType;
  public data ;
  currentUser;
  popoverTitle = 'Attention';
  popoverMessage = 'Êtes-vous sûr de vouloir effectuer cette opération?';
  confirmClicked = false;
  cancelClicked = false;
  @ViewChild('wordingTpl', { static: true }) wordingTpl: TemplateRef<any>;
  @ViewChild('orderTpl', { static: true }) orderTpl: TemplateRef<any>;
  @ViewChild('message_type_idTpl', { static: true }) message_type_idTpl: TemplateRef<any>;
  @ViewChild('survey_campaign_idTpl', { static: true }) survey_campaign_idTpl: TemplateRef<any>;
  @ViewChild('response_type_idTpl', { static: true }) response_type_idTpl: TemplateRef<any>;

  @ViewChild('wording') wording: ElementRef<any>;
  @ViewChild('order') order: ElementRef<any>;
  @ViewChild('message_type_id') message_type_id: ElementRef<any>;
  @ViewChild('survey_campaign_id') survey_campaign_id: ElementRef<any>;
  @ViewChild('response_type_id') response_type_id: ElementRef<any>;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;
  constructor(private messageservice: MessageService,
              private router: Router,
              private route: ActivatedRoute) {
                this.router = router;
              }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.columns = [
      { key: 'wording', title: 'Message', cellTemplate: this.wordingTpl },
      { key: 'order', title: 'Position', cellTemplate: this.orderTpl },
      { key: 'message_type_id.name', title: 'Type de message', cellTemplate: this.message_type_idTpl },
      { key: 'survey_campaign_id.name', title: 'Campagne', cellTemplate: this.survey_campaign_idTpl },
      { key: 'response_type_id.name', title: 'type de réponse', cellTemplate: this.response_type_idTpl },
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getMessages();
    this.getMessageType();
    this.getSurveyCampaign();
    this.getResponseType();
  }

  // Show All message
  public getMessages() {
    this.messageservice.getMessages().subscribe((data: Array<object>) => {
      // console.log(' Sms messages services', data);
      this.messages = data;
      this.data = this.messages;
    });
  }
  // show all messages types
  public getMessageType() {
    this.messageservice.getMessagetype().subscribe((data: Array<object>) => {
      // console.log('messages types', data);
      this.messageType = data;
    });
  }

  public getSurveyCampaign() {
    this.messageservice.getSurveyCampaign().subscribe((data: Array<object>) => {
      // console.log('messages types', data);
      this.surveyCampaign = data;
    });
  }

  public getResponseType() {
    this.messageservice.getResponseType().subscribe((data: Array<object>) => {
      // console.log('messages types', data);
      this.responseType = data;
    });
  }

  edit(rowIndex: number): void {
    this.selectRow = rowIndex;
    // console.log('row selected', this.selectRow);
  }

  update(row): void {
    this.data = [
      ...this.data.map((obj, index) => {
        if (index === this.selectRow) {
          return {
            _id: row._id,
            wording: this.wording.nativeElement.value,
            order: this.order.nativeElement.value,
            message_type_id: this.message_type_id.nativeElement.value,
            survey_campaign_id: this.survey_campaign_id.nativeElement.value,
            response_type_id: this.response_type_id.nativeElement.value,
          };
        }
       // console.log('object modified apres', obj._id);
        return obj;
      }),
    ];

   //console.log('data modified Avant', this.data[this.selectRow]);
   this.route.params.subscribe(params => {
    this.messageservice.update(this.data[this.selectRow]).subscribe(res => {
      //console.log('data modified apres', this.data[this.selectRow]);
  //    this.cdr.detectChanges()
  this.getMessages();
    });

});
    this.selectRow = -1;

  }

  removeMessage(val) {
    //console.log('val to remove', val);
    this.route.params.subscribe(params => {
      this.messageservice.remove(val).subscribe(res => {
    this.getMessages();
      });
  });
  }

}
