import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { MessageService } from '../../../services/message.service';   
import { SurveyCampaignService} from '../../../services/surveycampaign.service'; 
import { MessageTypeService} from '../../../services/message_type.service'; 
import { ResponseTypeService} from '../../../services/response_type.service'; 
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'ngx-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [SurveyCampaignService,MessageService, MessageTypeService,ResponseTypeService]
})
export class AddMessageComponent implements OnInit {
  messageForm: FormGroup;
  submittedMessage = false;
  messages;
  typemessages;
  surveycampaigns;
  typeresponses;

  constructor(private fb: FormBuilder,
              private surveycampaignservice : SurveyCampaignService,
              private messageservice : MessageService,
              private messagetypeservice : MessageTypeService,
              private responsetypeservice : ResponseTypeService,
              private router: Router,
              private route: ActivatedRoute ) { 
      this.messageForm = this.fb.group({
        wording : ['', Validators.required],
        order : ['', Validators.required],
        next_point : ['', Validators.required],
        survey_campaign_id : ['', Validators.required],
        message_type_id : ['', Validators.required],
        response_type_id : ['', Validators.required],
       });
    }

  ngOnInit(): void {
    this.getTypeMessages()
    this.getSurveyCampaigns()
    this.getTypeResponses()
  }

  // convenience getter for easy access to form fields
  get fMessage() {
    return this.messageForm.controls;
  }

   //Submit form
   onSubmitMessage(){
    this.submittedMessage = true;
    console.log("form Message",this.fMessage)
    if (this.messageForm.invalid){
      console.log('error Message form')
      return this.messageForm.controls;;
    };
    var form_tMessage = this.fMessage.wording.parent.value
    console.log("final value", form_tMessage )
    this.messageservice.createMessage(form_tMessage).subscribe((data: Array<object>) => {
      this.fMessage.wording.reset();
      console.log(" successfully create survey campaign")
      this.router.navigate(["pages/message"]);
   });   
  }

   // Show All message
   public getMessages() {
    this.messageservice.getMessages().subscribe((data: Array<object>) => {
       console.log(' Sms messages services', data);
       this.messages = data
    });    
  }

  // Show All type Message
  public getTypeMessages() {
    this.messagetypeservice.getTypeMessages().subscribe((data: Array<object>) => {
       console.log(' Sms types Messages services', data);
       this.typemessages = data
    });    
  }

  // Show All type Response
  public getTypeResponses() {
    this.responsetypeservice.getTypeResponses().subscribe((data: Array<object>) => {
       console.log(' Sms types Responses services', data);
       this.typeresponses = data
    });    
  }

  // Show All Survey campaigns
  public getSurveyCampaigns() {
    this.surveycampaignservice.getSurveyCampaign().subscribe((data: Array<object>) => {
       console.log(' survey campaign services', data);
       this.surveycampaigns = data
    });    
  }

}
