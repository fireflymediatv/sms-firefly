import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexMessageComponent } from './index/index.component';
import { AddMessageComponent } from './add/add.component';
import { GetMessageComponent } from './get/get.component';
import { EditMessageComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'message', component: IndexMessageComponent },
  { path: 'message/add', component: AddMessageComponent },
  { path: 'message/edit/:id', component: EditMessageComponent },
  { path: 'message/:id', component: GetMessageComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MessageRoutingModule { }

export const routedComponents = [
  IndexMessageComponent,
  AddMessageComponent ,
  EditMessageComponent,
  GetMessageComponent,
];
