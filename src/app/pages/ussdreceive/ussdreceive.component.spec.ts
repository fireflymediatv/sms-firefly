import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UssdreceiveComponent } from './ussdreceive.component';

describe('SmsreceiveComponent', () => {
  let component: UssdreceiveComponent;
  let fixture: ComponentFixture<UssdreceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UssdreceiveComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UssdreceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
