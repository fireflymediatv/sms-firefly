import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { SurveyCampaignService } from '../../../services/surveycampaign.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [SurveyCampaignService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public survey_campaign;
  public data ;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  public popoverTitle: String = 'Cette action est irréversible';
    public popoverMessage: String = 'Êtes-vous sûr de vouloir effectuer cette opération ?';
    public confirmClicked: Boolean = false;
    public cancelClicked: Boolean = false;
  statuses: NbComponentStatus[] = [ 'primary'];

  constructor(private curveycampaignservice:  SurveyCampaignService,
              private router: Router,
              private route: ActivatedRoute) {
                this.router = router;
               }

  ngOnInit(): void {
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [
      { key: 'name', title: 'Nom' },
      { key: 'keyword_id.name', title: 'Mot cle' },
      { key: 'campaign_code', title: 'Campagne Code' },
      { key: 'campaignName', title: 'Campagne' },
      { key: 'smsadpoint_id.name', title: 'Adpoint' },
      { key: 'smspartner_id.name', title: 'Partenaire' },
      { key: 'state', title: 'STATUS' },
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.getSurveyCampaign();
  }

   // Show All Sms partner
   public getSurveyCampaign() {
    this.curveycampaignservice.getSurveyCampaign().subscribe((data: Array<object>) => {
       // console.log(' Sms survey campaign', data);
       this.configuration.isLoading = false;
       this.survey_campaign = data;
       this.data = this.survey_campaign;
       //console.log('data campaign', data);
    });
  }

  public stopCampaign(value) {
    //console.log('value stop', value);
    this.curveycampaignservice.stopCampaign({'_id': value}).subscribe((data: Array<object>) => {
      // console.log(' Sms survey campaign', data);
      this.getSurveyCampaign();
      this.configuration.isLoading = false;
   });
  }

  public reloadCampaign(value) {
    //console.log('value stop', value);
    this.curveycampaignservice.reloadCampaign({'_id': value}).subscribe((data: Array<object>) => {
      // console.log(' Sms survey campaign', data);
      this.getSurveyCampaign();
      this.configuration.isLoading = false;
   });
  }

  public removeCampaign(value) {
    //console.log('valu to remove', value);
    this.route.params.subscribe(params => {
      this.curveycampaignservice.remove({_id: value}).subscribe(res => {
        this.getSurveyCampaign();
        this.configuration.isLoading = false;
      });
  });
  }

}
