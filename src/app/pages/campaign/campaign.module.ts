import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbPopoverModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CampaignRoutingModule, routedComponents  } from './campaign-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'ngx-easy-table';
import { RouterModule, Routes } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import { GetComponent } from './get/get.component';
import { EditComponent } from './edit/edit.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

export const campaignRoute: Routes = [
  { path: 'campaign', component: IndexComponent },
  { path: 'campaign/add', component: AddComponent },
  { path: 'campaign/edit/:id', component: EditComponent },
  { path: 'campaign/:id', component: GetComponent },
];

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    CommonModule,
    CampaignRoutingModule,
    TableModule,
    NbPopoverModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger',
           cancelButtonType: 'primary',
           confirmText: 'Effectuer',
           cancelText: 'Annuler' // set defaults here
    }),
    RouterModule.forChild(campaignRoute),
  ],
   declarations: [
    ...routedComponents,
  ],
})
export class CampaignModule { }


