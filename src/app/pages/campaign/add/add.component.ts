import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SmspartnerService } from '../../../services/smspartner.service';
import { SmsadpointService } from '../../../services/smsadpoint.service';
import { KeywordService } from '../../../services/keyword.service';
import { PrintService } from '../../../services/print.service';
import { CampaignService } from '../../../services/campaign.service';
import { SurveyCampaignService } from '../../../services/surveycampaign.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers : [SmspartnerService, SmsadpointService, KeywordService, PrintService, CampaignService, SurveyCampaignService],
})
export class AddComponent implements OnInit {
  surveyCampaignForm: FormGroup;
  surveyPartnerForm: FormGroup;
  surveyAdpointForm: FormGroup;
  submittedSurvey = false;
  submittedAdpoint = false;
  submittedPartner = false;
  successPartner = false;
  successAdpoint = false;
  partners;
  adpoints;
  keywords;
  customers;
  prints;
  campaigns;
  checked_sms = false;
  checked_ussd = false;
  SMS = 'SMS';
  USSD = 'USSD';
  val_sms; val_ussd; val_option;

  static parameters = [FormBuilder, SmspartnerService];
  constructor(private fb: FormBuilder,
              private smspartnerservice: SmspartnerService,
              private smsadpointservice: SmsadpointService,
              private keywordservice: KeywordService,
              private campaignservice: CampaignService,
              private printservice: PrintService,
              private surveycampaignservice: SurveyCampaignService,
              private router: Router,
              private route: ActivatedRoute ) {
    this.surveyPartnerForm = this.fb.group({
       name : ['', Validators.required],
      });
    this.surveyAdpointForm = this.fb.group({
      name : ['', Validators.required],
      });

    this.surveyCampaignForm = this.fb.group({
      name : ['', Validators.required],
      incentive : ['', Validators.required],
      engagement_question : ['', Validators.required],
      incentive_value : ['', Validators.required],
      format : ['', Validators.required],
      smspartner_id : ['', Validators.required],
      smsadpoint_id : ['', Validators.required],
      keyword_id : ['', Validators.required],
      campaign_id : [],
      nb_participate : ['', Validators.required],
      frequence_day : ['', Validators.required],
      print_id : [],
      campaign_code : [],
      customerId: ['', Validators.required],
     });

     this.val_option = '';
  }

  // convenience getter for easy access to form fields
  get fsurveyCanpaign() {
    return this.surveyCampaignForm.controls;
  }

  get fsurveyPartner() {
    return this.surveyPartnerForm.controls;
  }
  get fsurveyAdpoint() {
    return this.surveyAdpointForm.controls;
  }

  // Submit form adpoint
  onSubmitSurveyAdpoint() {
    this.submittedAdpoint = true;
    if (this.surveyAdpointForm.invalid) {
      console.log('error adpoint form');
      return this.surveyAdpointForm.controls; 
    }
    const form_adpoint = this.fsurveyAdpoint.name.parent.value;

    this.smsadpointservice.create(form_adpoint).subscribe((data: Array<object>) => {
     // this.fsurveyAdpoint.name.reset();
      this.successAdpoint = true;
     // console.log(" successfully create adpoint")
   });

   }

   // Submit form partner
   onSubmitSurveyPartner() {
    this.submittedPartner = true;
    if (this.surveyPartnerForm.invalid) {
      console.log('error partner form');
      return this.surveyPartnerForm.controls; 
    }
    const form_partner = this.fsurveyPartner.name.parent.value;

    this.smspartnerservice.create(form_partner).subscribe((data: Array<object>) => {
     // this.fsurveyPartner.name.reset();
      this.successPartner = true;
     // console.log(" successfully create partner")
   });

   }

     // Submit form
   onSubmitSurveyCampaign() {
        let date;
        this.val_option = '';
        this.submittedSurvey = true;
        console.log('total value', this.fsurveyCanpaign );
        if (this.surveyCampaignForm.invalid) {
         // console.log('error partner form')
          return this.surveyCampaignForm.controls; 
        }
        if (this.checked_sms)
           this.val_option = this.SMS;
        if (this.checked_ussd)
           this.val_option = this.val_option + ' ' +  this.USSD;
        const form_survey = this.fsurveyCanpaign.name.parent.value;
        Object.defineProperty(form_survey, 'interaction_option', {value: this.val_option, enumerable: true,
          configurable: true});

        // console.log("final value",form_survey )
        this.surveycampaignservice.createSurvey(form_survey).subscribe((data: Array<object>) => {
          this.fsurveyCanpaign.name.reset();
          this.fsurveyCanpaign.keyword_id.reset();
         // console.log(" successfully create campaign")
          this.router.navigate(['pages/campaign']);
       });
    }

  ngOnInit() {
    this.getAdpoints();
    this.getPartners();
    this.getKeywords();
    this.getPrints();
    this.getCampaigns();
    this.getAllCustomers();
  }

  toggle_sms(checked: boolean) {
    this.checked_sms = checked;
  }

  toggle_ussd(checked: boolean) {
    this.checked_ussd = checked;
    // console.log(" val ussd", this.checked_ussd)
  }

   // Show All Sms partner
   public getPartners() {
    this.smspartnerservice.getSmspartners().subscribe((data: Array<object>) => {
       // console.log(' Sms partners services', data);
       this.partners = data;
    });
  }

  // Show All Customers
  public getAllCustomers() {
    this.surveycampaignservice.getAllCustomer().subscribe((data: Array<object>) => {
       // console.log(' Sms partners services', data);
       this.customers = data;
    });
  }
  // Show All adpint
  public getAdpoints() {
    this.smsadpointservice.getSmsadpoints().subscribe((data: Array<object>) => {
       // console.log(' Sms adpoints services', data);
       this.adpoints = data;
    });
  }

  // Show All keyword
  public getKeywords() {
    this.keywordservice.getKeywords().subscribe((data: Array<object>) => {
       // console.log(' Sms keywords services', data);
       this.keywords = data;
    });
  }

  // Show All prints
  public getPrints() {
    this.printservice.getPrints().subscribe((data: Array<object>) => {
       // console.log(' Sms print services', data);
       this.prints = data;
    });
  }

   // Show All campaigns
   public getCampaigns() {
    this.campaignservice.getCampaigns().subscribe((data: Array<object>) => {
       // console.log(' campaign services', data);
       this.campaigns = data;
    });
  }

}
