import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CampaignComponent } from './campaign.component';
import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import { GetComponent } from './get/get.component';
import { EditComponent } from './edit/edit.component';

export const campaignRoute: Routes = [{
     path: '',
     component: CampaignComponent,
     children: [
          { path: 'campaign', component: IndexComponent },
          { path: 'campaign/add', component: AddComponent },
          { path: 'campaign/edit/:id', component: EditComponent },
          { path: 'campaign/:id', component: GetComponent },
        ],
      }];

@NgModule({
  imports: [
    RouterModule.forChild(campaignRoute),
  ],
  exports: [
    RouterModule,
  ],
})
export class CampaignRoutingModule {
}

export const routedComponents = [
  IndexComponent,
  AddComponent,
  GetComponent,
  EditComponent,
];
