import { Component } from '@angular/core';
import {OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';
import { NbAccessChecker } from '@nebular/security';
import { NbMenuItem } from '@nebular/theme';
const roleUser = 'user';
@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  currentUser;
  menu = MENU_ITEMS;
  constructor(private accessChecker: NbAccessChecker) {
  }

  ngOnInit(): void {
    this.authMenuItems();
  }

  authMenuItems() {
     this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.menu.forEach(item => {
      this.authMenuItem(item);
    });
  }

  authMenuItem(menuItem: NbMenuItem) {
    if (menuItem.data && menuItem.data['permission'] && menuItem.data['resource']) {
      this.accessChecker.isGranted(menuItem.data['permission'], menuItem.data['resource']).subscribe(granted => {
        console.log(' --- granted ---', granted);
        menuItem.hidden = !granted;
      });
    } else {
       if (this.currentUser.role == roleUser) {   // si c'est un 'admin' user avc comme role == user
           menuItem.hidden = false;
        } else {
          menuItem.hidden = true;  // default
        }
    }
    if (!menuItem.hidden && menuItem.children != null) {
      menuItem.children.forEach(item => {
        if (item.data && item.data['permission'] && item.data['resource']) {
          this.accessChecker.isGranted(item.data['permission'], item.data['resource']).subscribe(granted => {
            item.hidden = !granted;
          });
        } else {
            // if child item do not config any `data.permission` and `data.resource` just inherit parent item's config
              item.hidden = menuItem.hidden;
        }
      });
    }
  }


}
