import {Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { AudienceService } from '../../services/audience.service';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDateService } from '@nebular/theme';
import { NbThemeService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { SurveyCampaignService} from '../../services/surveycampaign.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

export interface UserSms {
  telephone: string;
  date: string;
  short_code: string;
  message: string;
}


@Component({
  selector: 'ngx-audience',
  templateUrl: './audience.component.html',
  styleUrls: ['./audience.component.scss'],
  providers : [AudienceService, SurveyCampaignService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AudienceComponent implements OnInit, OnDestroy {
      options: any = {};
      public configuration: Config;
      public columns: Columns[];
      themeSubscription: any;
      route;
      data_sms;
      kpi;
      chartDatas;
      chartDataInTheWeek;
      surveycampaigns;
      submittedSurvey;
      data_sms_csv;
      data_sms_chart;
      data_map_chart;
      dateSmsForm: FormGroup;
      surveyForm: FormGroup;
      maxDate: Date;
      min: Date;
      max: Date;
      date;
      value;
      diffInDays;
      firstDate;
      secondDate;
      submittedDate = false;
      chartConfigSmsByScreen: Object;
      dataSourceSmsByScreen = {};
      chartConfigRateInSevenDays: Object;
      dataSourceRateInSevenDays = {};
      public resultat = [];

      static parameters = [AudienceService, SurveyCampaignService, FormBuilder, NbDateService];

      constructor(  private audienceservice: AudienceService,
                    private fb: FormBuilder,
                    private dateService: NbDateService<Date>) {

         this.dateSmsForm = this.fb.group({
          data_date_sms : ['', Validators.required],
         });

         this.surveyForm = this.fb.group({
          survey_campaign_id : ['', Validators.required],
         });

         this.max = this.dateService.today();
         this.min = this.dateService.addMonth(this.dateService.today(), -36);
      }

        // convenience getter for easy access to form fields
    get fDateProbe() {
      return this.dateSmsForm.controls;
    }

    // convenience getter for easy access to form fields
    get fSurvey() {
      return this.surveyForm.controls;
    }

    ngOnInit(): void {
     // this.configuration.isLoading = true;
     this.configuration = { ...DefaultConfig };
     this.configuration.searchEnabled = true;
       this.columns = [
         { key: 'week_level', title: 'Semaine' },
         { key: 'today', title: 'Date' },
         { key: 'operation', title: 'Opération' },
         { key: 'screen', title: 'Ecran' },
         { key: 'display_by_day', title: 'Aff jour' },
         { key: 'display_kpi_sms', title: 'KPI-SMS/Affichage' },
         { key: 'sms_cumulates', title: 'Sms cumulé', cssClass: { includeHeader: false, name: 'blue' } },
         { key: 'sms_day', title: 'Sms jour' },
         { key: 'sms_kpi_by_screen', title: 'KPI - Sms/Ecran' },
         { key: 'avg_last_30_day', title: 'AVG last 30 days', cssClass: { includeHeader: true, name: 'pink' }  },
       ];
        this.getKpi();
        // this.chartSmsByScreen();
        this.updateMyChartData();
        this.updateMyChartDataInTheWeek();
        // this.chartSmsAverageInSevenDays();
        this.getChartData();
    }


     // Show All Survey campaigns
  public getKpi() {
    this.audienceservice.getKpis().subscribe((data: Array<object>) => {
       this.kpi = data;
    });
  }

  // Get Chart Datas
  public getChartData() {
    this.audienceservice.getChartAudiences(this.date).subscribe((res) => {
       this.chartDatas = res[0];
       this.chartDataInTheWeek = res[1];
       // console.log('chart', this.chartDatas);
       this.updateMyChartData();
       this.updateMyChartDataInTheWeek();
    });
  }

  updateMyChartData() {
  // fusion chart
  this.chartConfigSmsByScreen = {
   width: '810',
   height: '400',
   type: 'line',
   dataFormat: 'json',
};

this.dataSourceSmsByScreen = {
   'chart': {
    caption: 'SMS par écran',
    subcaption: '',
    yaxisname: 'Nombre',
    xaxisname: 'Date',
    forceaxislimits: '1',
    pixelsperpoint: '0',
    pixelsperlabel: '30',
    compactdatamode: '1',
    theme: 'fusion',
   },
   'data': this.chartDatas,
 };

}

updateMyChartDataInTheWeek() {
  // fusion chart
  this.chartConfigRateInSevenDays = {
   width: '810',
   height: '400',
   type: 'line',
   dataFormat: 'json',
};

this.dataSourceRateInSevenDays = {
   'chart': {
    caption: 'SMS par écran - Moyenne mobile 7 jours',
    subcaption: '',
    yaxisname: 'Nombre',
    xaxisname: 'Date',
    forceaxislimits: '1',
    pixelsperpoint: '0',
    pixelsperlabel: '30',
    compactdatamode: '1',
    theme: 'fusion',
   },
   'data': this.chartDataInTheWeek,
 };

}

onSubmitDateInterval() {
  this.submittedDate = true;
  if (this.dateSmsForm.invalid) {
    console.log('error date');
    return this.dateSmsForm.controls;
  }
  this.date = {
    'start_date' : new Date(this.fDateProbe.data_date_sms.value.start), // this.datePipe.transform(this.fDateProbe.data_date_sms.value.start, 'yyyy-MM-dd'),
      'end_date' : new Date(this.fDateProbe.data_date_sms.value.end), // this.datePipe.transform(this.fDateProbe.data_date_sms.value.end, 'yyyy-MM-dd')
  };
  this.firstDate = moment(this.date.start_date);
this.secondDate = moment(this.date.end_date);
this.diffInDays = Math.abs(this.firstDate.diff(this.secondDate, 'days'));
  this.getChartData();
 }

    ngOnDestroy(): void {
      console.log('--OnDestrooy---');
      if (this.themeSubscription)
        this.themeSubscription.unsubscribe();
     //  this.router.navigateByUrl('/auth/login');
    }


    }

