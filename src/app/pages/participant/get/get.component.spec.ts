import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetKeywordComponent } from './get.component';

describe('GetKeywordComponent', () => {
  let component: GetKeywordComponent;
  let fixture: ComponentFixture<GetKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
