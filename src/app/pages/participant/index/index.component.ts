import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { ParticipantService } from '../../../services/participant.service';
import { NbComponentShape, NbComponentSize, NbComponentStatus, NbThemeService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ExportToCsv } from 'export-to-csv';
import { DatePipe } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { Row } from 'ng2-smart-table/lib/lib/data-set/row';
registerLocaleData(localeFr, 'fr');


const userRole = 'user';
interface POI {
  tabChoiceLine: string | any ;
  tabChoicePoi: string | any ;
}

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers : [ParticipantService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexParticipantComponent implements OnInit {
  public configuration: Config;
  public columns: Columns[];
  public keywords;
  options: any = {};
  themeSubscription: any;
  public data ;
  public phoneN;
  chartGenderLegend;
  chartGenderValue;
  dropdownLines = {};
  updatePhone = [];
  public city;
  public survey;
  public lines;
  submittedCsv = false;
  dataSourceAudience;
  dataSourceOptIn;
  dataSourceNull;
  chartConfigAudience: {};
  chartConfigNull: {};
  chartConfigOptIn: {};
  numAutorised;
  numMaxQualified;
  numIntQualified;
  numNullQualified;
  rateAutorised;
  rateMaxQualified;
  rateIntQualified;
  rateNullQualified;
  qualifiedAudience;
  nullQualified;
  optIn;
  allResidence;
  hide_value = false;
  critere_value;
  qualifiedPartially;
  submittedDate = false;
  submittedCritere = false;
  dateSmsForm: FormGroup;
  dateForm: FormGroup;
  csvForm: FormGroup;
  critereForm: FormGroup;
  dateSelectedForm: FormGroup;
  popupForm: FormGroup;
  date;
  period;
  popup = false;
  critere;
  critere_data;
  Date;
  valClicked;
  fileName;
  currentUser;
  data_participant_csv;
  data_participant_pdf;
  submittedPeriod;
  jpt;
  public jobTitle = ['Etudiant/élève', 'Ménagère', 'Enseignant/Professeur', 'Artisan', 'Commerçant', 'Travailleur secteur public',
                    'Travailleur secteur privé', 'Entrepreneur', 'Chômeur', 'Personnel médical', 'Homme de tenue', 'sportif',
                    'musicien', 'personnel transport', 'non renseigné', 'autre'];
  public periodTitle = ['jour', 'semaine', 'mois'];
  public critere_list = ['age', 'ville', 'sexe', 'profession', 'lieu', 'opt_in'];
  public critere_value_age = ['-20', '20 - 29', '30 - 39', '+40'];
  public critere_value_optin = ['Oui', 'Non'];
  public critere_value_gender = ['Homme', 'Femme'];
  public critere_value_region = ['DAKAR', 'THIES', 'DIOURBEL', 'FATICK', 'KAOLACK', 'KAFFRINE', 'KOLDA', 'SAINT-LOUIS', 'LOUGA', 'ZIGUINCHOR', 'TOUBA', 'MBOUR', 'SEDHIOU', 'TAMBACOUNDA']
  hide = false;
  loading_autorised = false;
  loading_max = false;
  loading_int = false;
  loading_null = false;
  loading_all = false;
  loading_critere = false;
  submittedPopup = false;
  editRow: number;
  @ViewChild('actionTpl', { static: true }) actionTpl: TemplateRef<any>;
  statuses: NbComponentStatus[] = [ 'primary'];
  selectRow: number;
  numberArray = [];
  submittedLinePrev = false;
  selected = [];
  statusPopup;
  selectedCampaign = [];
  liness = []; // for headings
  linesR = [];
  popoverTitle = 'Attention';
  popoverMessage = 'Êtes-vous sûr de vouloir effectuer cette opération?';
  confirmClicked = false;
  cancelClicked = false;
  @ViewChild('firstNameTpl', { static: true }) firstNameTpl: TemplateRef<any>;
  @ViewChild('lastNameTpl', { static: true }) lastNameTpl: TemplateRef<any>;
  @ViewChild('phone_numberTpl', { static: true }) phone_numberTpl: TemplateRef<any>;
  @ViewChild('ageTpl', { static: true }) ageTpl: TemplateRef<any>;
  @ViewChild('genderTpl', { static: true }) genderTpl: TemplateRef<any>;
  @ViewChild('cityIdTpl', { static: true }) cityIdTpl: TemplateRef<any>;
  @ViewChild('professionTpl', { static: true }) professionTpl: TemplateRef<any>;
  @ViewChild('residenceTpl', { static: true }) residenceTpl: TemplateRef<any>;
  @ViewChild('lineTpl', { static: true }) lineTpl: TemplateRef<any>;
  @ViewChild('autorisedNumberTpl', { static: true }) autorisedNumberTpl: TemplateRef<any>;
  @ViewChild('activationCampaignTpl', { static: true }) activationCampaignTpl: TemplateRef<any>;
  @ViewChild('departureStopTpl', { static: true }) departureStopTpl: TemplateRef<any>;
  @ViewChild('arrivalStopTpl', { static: true }) arrivalStopTpl: TemplateRef<any>;
  @ViewChild('lastCampaignTpl', { static: true }) lastCampaignTpl: TemplateRef<any>;
  @ViewChild('qualificationDateTpl', { static: true }) qualificationDateTpl: TemplateRef<any>;

  @ViewChild('firstName') firstName: ElementRef<any>;
  @ViewChild('lastName') lastName: ElementRef<any>;
  @ViewChild('phone_number') phone_number: ElementRef<any>;
  @ViewChild('age') age: ElementRef<any>;
  @ViewChild('gender') gender: ElementRef<any>;
  @ViewChild('cityId') cityId: ElementRef<any>;
  @ViewChild('profession') profession: ElementRef<any>;
  @ViewChild('residence') residence: ElementRef<any>;
  @ViewChild('line') line: ElementRef<any>;
  @ViewChild('autorisedNumber') autorisedNumber: ElementRef<any>;
  @ViewChild('activationCampaign') activationCampaign: ElementRef<any>;
  @ViewChild('departureStop') departureStop: ElementRef<any>;
  @ViewChild('arrivalStop') arrivalStop: ElementRef<any>;
  @ViewChild('lastCampaign') lastCampaign: ElementRef<any>;
  @ViewChild('qualificationDate') qualificationDate: ElementRef<any>;
  @ViewChild('myModal', { static: false }) myModal: ElementRef;

  constructor(private participantService:  ParticipantService,
              private router: Router,
              private route: ActivatedRoute,
              private theme: NbThemeService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private cdr: ChangeDetectorRef) {
                this.router = router;
                this.dateSmsForm = this.fb.group({
                  campaign : ['', Validators.required],
                  date : ['', Validators.required],
                 });
                this.dateForm = this.fb.group({
                  Date : ['', Validators.required]
                });
                this.critereForm = this.fb.group({
                  criteria : ['', Validators.required],
                  value : ['', Validators.required]
                });
                this.popupForm = this.fb.group({
                  period : ['', Validators.required]
                });
               }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;
    this.configuration.isLoading = true;
    this.columns = [

      { key: 'phone_number', title: 'Téléphone', cellTemplate: this.phone_numberTpl },
      { key: 'age', title: 'Age', cellTemplate: this.ageTpl },
      { key: 'gender', title: 'genre', cellTemplate: this.genderTpl },
      { key: 'profession', title: 'Profession', cellTemplate: this.professionTpl },
      { key: 'city', title: 'Ville', cellTemplate: this.cityIdTpl },
      { key: 'residence', title: 'Lieu', cellTemplate: this.residenceTpl },
      { key: 'usedLine', title: 'Ligne utilisée', cellTemplate: this.lineTpl },
      { key: 'departureStop', title: 'Arrêt Départ', cellTemplate: this.departureStopTpl },
      { key: 'arrivalStop', title: 'Arrêt D\'arrivée', cellTemplate: this.arrivalStopTpl },
      { key: 'autorisedNumber', title: 'Numéro autorisé', cellTemplate: this.autorisedNumberTpl },
      { key: 'lastCampaign', title: 'Dernière campagne', cellTemplate: this.lastCampaignTpl },
      { key: 'qualificationDate', title: 'Dernière qualification', cellTemplate: this.qualificationDateTpl },
      //{ key: 'activationCampaign', title: 'Campagne d\'activation', cellTemplate: this.activationCampaignTpl }, //<!--1-->
      { key: 'action', title: 'Actions', cellTemplate: this.actionTpl },
    ];
    this.numberArray = [];
    this.getQualifiedParticipants();
    this.getParticipants();
    this.getCity();
    this.getLine();
    this.getResidence();
    this.getSurveyCampaign();
  }

   // Show All Sms partner
   public getParticipants() {
    this.participantService.get().subscribe((data: Array<object>) => {
       // console.log(' participants', data);
       this.configuration.isLoading = false;
       this.keywords = data;
       this.data = this.keywords;
    });
  }

  public qualifiedParticipantByPeriod() {
    // this.loading_data = true;
   this.participantService.qualifiedParticipantByPeriod(this.date).subscribe((data: Array<object>) => {
      this.qualifiedAudience = data;
      // this.loading_data = false;
      this.chartQualifiedByPeriod();
   });
   this.participantService.nullRepartition(this.date).subscribe((res: Array<object>) => {
    this.nullQualified = res;
    // this.loading_data = false;
    this.chartNullQualifiedByPeriod();
 });
  this.participantService.autorisedRepartition(this.date).subscribe((data: Array<object>) => {
    this.optIn = data;
    // this.loading_data = false;
    this.chartOptInByPeriod();
});
 }
 onSubmitPeriod() {
  this.submittedPeriod = true;
  if (this.dateForm.invalid) {
    console.log('error date');
    return this.dateForm.controls;
  }
  this.configuration.isLoading = true;
  this.Date = {
      'start_date' : this.datePipe.transform(this.fPeriodProbe.Date.value.start),
      'end_date' : this.datePipe.transform(this.fPeriodProbe.Date.value.end)
  };
  //console.log('success date', this.Date);
  this.participantService.filterByLastMessageDate(this.Date).subscribe((data: Array<object>) => {
    this.keywords = data;
    this.data = this.keywords;
    this.configuration.isLoading = false;
  });
}

 chartQualifiedByPeriod() {
  // console.log('enter chart', this.residenceRepartition);
   // fusion chart
   this.chartConfigAudience = {
    width: '300',
    // height: '200',
    type: 'column2d',
    dataFormat: 'json',
 };

 this.dataSourceAudience = {
    'chart': {
     caption: 'Répartition de l\'audience qualifiée',
     subcaption: '',
     yaxisname: '',
     xaxisname: 'Période',
     forceaxislimits: '1',
     pixelsperpoint: '0',
     pixelsperlabel: '30',
     compactdatamode: '1',
     theme: 'fusion',
    },
    'data': this.qualifiedAudience,
  };
 }

 chartNullQualifiedByPeriod() {
   // fusion chart
   this.chartConfigNull = {
    width: '300',
    type: 'line',
    dataFormat: 'json',
 };

 this.dataSourceNull = {
    'chart': {
     caption: 'Répartition participants à qualification null',
     subcaption: '',
     yaxisname: '',
     xaxisname: 'Période',
     forceaxislimits: '1',
     pixelsperpoint: '0',
     pixelsperlabel: '30',
     compactdatamode: '1',
     theme: 'fusion',
    },
    'data': this.nullQualified,
  };
 }

 chartOptInByPeriod() {
  // fusion chart
  this.chartConfigOptIn = {
   width: '300',
   type: 'line',
   dataFormat: 'json',
};

this.dataSourceOptIn = {
   'chart': {
    caption: 'Répartition participants Opt-in',
    subcaption: '',
    yaxisname: '',
    xaxisname: 'Période',
    forceaxislimits: '1',
    pixelsperpoint: '0',
    pixelsperlabel: '30',
    compactdatamode: '1',
    theme: 'fusion',
   },
   'data': this.optIn,
 };
}

get fPeriodProbe(){
  return this.dateForm.controls;
}
 get fDateProbe() {
  return this.dateSmsForm.controls;
}
get fCsvFile() {
  return this.csvForm.controls;
}
get fCritere(){
  return this.critereForm.controls;
}
get fPopup(){
  return this.popupForm.controls;
}

onSubmitValue(val) {
  if(val == null || val == undefined || val =='') {
    return this.critereForm.controls;
  } else {
    switch(val) {
      case 'age':
        this.critere_value = this.critere_value_age;
        break;
      case 'sexe':
          this.critere_value = this.critere_value_gender;
          break;
      case 'profession':
            this.critere_value = this.jobTitle;
            break;
      case 'opt_in':
            this.critere_value = this.critere_value_optin;
            break;
      case 'ville':
            this.critere_value = this.critere_value_region;
            break;
      case 'lieu':
            this.critere_value = this.allResidence;
            break;
    }
    this.hide_value = true;
  }
}

onSubmitCritere() {
  this.submittedCritere = true;
  if (this.critereForm.invalid) {
    //console.log('error date');
    return this.critereForm.controls;
  }
  this.loading_critere = true;
  this.critere = {
      'critere' : this.fCritere.criteria.value,
      'value' : this.fCritere.value.value,
  };

    this.participantService.searchByCriteria(this.critere).subscribe((data: Array<object>) => {
      this.critere_data  = data;
      this.downloadFile(this.critere_data, this.critere.critere+'-'+this.critere.value);
      this.fCritere.criteria.reset();
      this.fCritere.value.reset();
      this.loading_critere = false;
  });
}
 onSubmitDateInterval() {
  this.submittedDate = true;
  if (this.dateSmsForm.invalid) {
    console.log('error date');
    return this.dateSmsForm.controls;
  }
  this.date = {
      'campaign' : this.fDateProbe.campaign.value,
      'start_date' : this.datePipe.transform(this.fDateProbe.date.value.start),
      'end_date' : this.datePipe.transform(this.fDateProbe.date.value.end),
  };
  //console.log('success date', this.date);
  this.qualifiedParticipantByPeriod();
}

popupStatus(val) {
  this.popup = true;
  this.statusPopup = val;
}

onSubmitPopupDate() {
  //console.log(val);
  this.submittedPopup = true;
  if (this.popupForm.invalid) {
    console.log('error date');
    return this.popupForm.controls;
  }
  this.period = {
      'start_date' : this.datePipe.transform(this.fPopup.period.value.start),
      'end_date' : this.datePipe.transform(this.fPopup.period.value.end),
  };
  switch(this.statusPopup) {
    case 'maximum':
      this.onClickButtonMax();
      break;
    case 'intermediaire':
      this.onClickButtonInt();
      break;
    case 'null':
      this.onClickButtonNull();
      break;
    case 'autorised':
      this.onClickButtonAutorised();
      break;
    }
    this.popup = false;
    this.fPopup.period.reset();
}

  public getQualifiedParticipants() {
    const obj = '';
    this.participantService.qualifiedParticipant(obj).subscribe((data: Array<object>) => {
       console.log(' participants qualified', data);
       this.configuration.isLoading = false;
       this.numAutorised = data[0];
       this.numMaxQualified = data[1];
       this.numIntQualified = data[2];
       this.numNullQualified = data[3];
       this.rateAutorised = data[4];
       this.rateMaxQualified = data[5];
       this.rateIntQualified = data[6];
       this.rateNullQualified = data[7];
    });
  }

  // public showQualifiedParticipants() {
  //   const obj = '';
  //   this.participantService.showQualifiedParticipant(obj).subscribe((data: Array<object>) => {
  //      // console.log(' participants qualified', data);
  //      this.configuration.isLoading = false;
  //      this.qualifiedParticipant = data[0];
  //      this.autorisedParticipant = data[1];
  //      this.qualifiedPartially = data[2];
  //   });
  // }

  public onClickButtonAutorised() {
    this.loading_autorised = true;
    const obj = 'autorised';
    this.participantService.qualifiedParticipant({value:obj, period: this.period}).subscribe((data: Array<object>) => {
      this.keywords = data;
       this.data = this.keywords;
       this.loading_autorised = false;
   });
  }

  public onClickButtonMax() {
    this.loading_max = true;
    const obj = 'maximum';
    this.participantService.qualifiedParticipant({value:obj, period: this.period}).subscribe((data: Array<object>) => {
      this.keywords = data;
       this.data = this.keywords;
       console.log('maximum',data);
       this.loading_max = false;
   });
  }

  public onClickButtonInt() {
    this.loading_int = true;
    const obj = 'intermédiaire';
    this.participantService.qualifiedParticipant({value:obj, period: this.period}).subscribe((data: Array<object>) => {
      this.keywords = data;
       this.data = this.keywords;
       this.loading_int = false;
   });
  }

  public onClickButtonNull() {
    this.loading_null = true;
    const obj = 'null';
    this.participantService.qualifiedParticipant({value:obj, period: this.period}).subscribe((data: Array<object>) => {
      this.keywords = data;
       this.data = this.keywords;
       this.loading_null = false;
   });
  }


  public getCity() {
    this.participantService.getCity().subscribe((data: Array<object>) => {
       this.configuration.isLoading = false;
      this.city = data;
       // console.log('city', this.city);
    });
  }

  public getResidence() {
    this.participantService.getParticipantResidence().subscribe((data: Array<object>) => {
       this.configuration.isLoading = false;
      this.allResidence = data;
       // console.log('city', this.city);
    });
  }

  public getLine() {
    this.participantService.getLine().subscribe((data: Array<object>) => {
       this.configuration.isLoading = false;
      this.lines = data;
       // console.log('city', this.city);
    });
  }

  public getSurveyCampaign() {
    this.participantService.getSurveyCampaign().subscribe((data: Array<object>) => {
       this.configuration.isLoading = false;
      this.survey = data;
       // console.log('city', this.city);
    });
  }

  getSelectedValue() {
    console.log(this.selected);
  }
  edit(rowIndex: number): void {
    this.editRow = rowIndex;
  }

  update(row): void {
    this.data = [
      ...this.data.map((obj, index) => {
        if (index === this.editRow) {
          // console.log('object modified', obj._id);
          return {
            _id: row._id,
            phone_number: this.phone_number.nativeElement.value,
            age: this.age.nativeElement.value,
            gender: this.gender.nativeElement.value,
            cityId: this.cityId.nativeElement.value,
            profession: this.profession.nativeElement.value,
            residence: this.residence.nativeElement.value,
            line: this.selected,
            autorisedNumber: this.autorisedNumber.nativeElement.value,
            activationCampaign: this.selectedCampaign,
            departureStop: this.departureStop.nativeElement.value,
            arrivalStop: this.arrivalStop.nativeElement.value,
          };
        }
       // console.log('object modified apres', obj._id);
        return obj;
      }),
    ];
    // console.log('data modified', this.data);
   //console.log('data modified Avant', this.data[this.editRow]);
   this.route.params.subscribe(params => {
    this.participantService.update(this.data[this.editRow]).subscribe(res => {
      //console.log('data modified apres', this.data[this.editRow]);
  //    this.cdr.detectChanges()
       this.getParticipants();
    });
});
    this.editRow = -1;
  }

  removeParticipant(val) {
    //console.log('val to remove', val);
    this.route.params.subscribe(params => {
      this.participantService.remove(val).subscribe(res => {
    this.getParticipants();
      });
  });
  }


 // download CSV
 ConvertToCSV(objArray, headerList) {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let str = '';
  let row = 'S.No,';
  for (let index in headerList) {
   row += headerList[index] + ',';
  }
  row = row.slice(0, -1);
  str += row + '\r\n';
  for (let i = 0; i < array.length; i++) {
   let line = (i+1)+'';
   for (let index in headerList) {
    let head = headerList[index];
    line += ',' + array[i][head];
   }
   str += line + '\r\n';
  }
  return str;
 }
 downloadFile(data, filename) {
  let csvData = this.ConvertToCSV(data, ['lastCampaign','age', 'arrivalStop', 'autorisedNumber','city', 'departureStop','gender', 'lastMessageDate', 'phone_number','profession', 'qualificationDate', 'residence', 'line']);
  //console.log(csvData)
  let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
  let dwldLink = document.createElement("a");
  let url = URL.createObjectURL(blob);
  let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
  if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
  }
  dwldLink.setAttribute("href", url);
  dwldLink.setAttribute("download", filename + ".csv");
  dwldLink.style.visibility = "hidden";
  document.body.appendChild(dwldLink);
  dwldLink.click();
  document.body.removeChild(dwldLink);
}
 download(value) {

  if(value == 'null'){
    this.loading_null = true;
    const obj = value;
    this.participantService.qualifiedParticipant({value:obj}).subscribe((data: Array<object>) => {
      this.data_participant_csv  = data;
      this.downloadFile(this.data_participant_csv, 'Participants à qualifiaction null')
      //csvExporter.generateCsv(this.data_participant_csv);
      this.loading_null = false;
 });

}
else if(value == 'intermédiaire'){
  this.loading_int = true;
    const obj = value;
    this.participantService.qualifiedParticipant({value:obj}).subscribe((data: Array<object>) => {
      this.data_participant_csv  = data;
      this.downloadFile(this.data_participant_csv, 'Participants à qualifiaction intermédiaire')
      this.loading_int = false;
  });

}
if(value == 'maximum'){
  this.loading_max = true;
    const obj = value;
    this.participantService.qualifiedParticipant({value:obj}).subscribe((data: Array<object>) => {
      this.data_participant_csv  = data;
      this.downloadFile(this.data_participant_csv, 'Participants à qualifiaction maximum')
      this.loading_max = false;
  });

}
else if(value == 'autorised'){
  this.loading_autorised = true;
    const obj = value;
    this.participantService.qualifiedParticipant({value:obj}).subscribe((data: Array<object>) => {
      this.data_participant_csv  = data;
      this.downloadFile(this.data_participant_csv, 'Participants opt-in')
      this.loading_autorised = false;
  });

}
else if(value == 'all'){
  this.loading_all = true;
  const obj = value;
  this.participantService.get().subscribe((data: Array<any>) => {
    this.data_participant_csv  = data;
    this.downloadFile(this.data_participant_csv, 'Tous les Participants')
    this.loading_all = false;
});

}
}


}

