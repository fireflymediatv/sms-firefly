import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';   
import { ActivatedRoute, Router } from '@angular/router';
import { ParticipantService } from '../../../services/participant.service';

@Component({
  selector: 'ngx-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers : [ParticipantService]
})
export class AddParticipantComponent implements OnInit {
  participantForm: FormGroup;
  submitted = false;
  keywords;
  typekeywords;

  constructor(private fb: FormBuilder,
              private participantService : ParticipantService,
              private router: Router,
              private route: ActivatedRoute ) { 
    this.participantForm = this.fb.group({
      firstName : ['', Validators.required],
      lastName : ['', Validators.required],
      phone_number : ['', Validators.required],
      age : ['', Validators.required],
      gender : ['', Validators.required],
     });

  }

  // convenience getter for easy access to form fields
  get fKeyword() {
    return this.participantForm.controls;
  }

     //Submit form
   onSubmit(){
        this.submitted = true;
        console.log('form keyword',this.fKeyword)
        if (this.participantForm.invalid){
          console.log('error keyword form')
          return this.participantForm.controls;
        };
        console.log('final value', this.participantForm.value );
        this.participantService.create(this.participantForm.value).subscribe((data: Array<object>) => {

          console.log(' successfully create participant');
          this.router.navigate(['pages/participant']);
       });   
    }

  ngOnInit() {
    //this.getParticipants();
  }

  // Show All participants
  /*public getParticipants() {
    this.participantService.get().subscribe((data: Array<object>) => {
       console.log(' participants', data);
       this.keywords = data
    });    
  }*/


}
