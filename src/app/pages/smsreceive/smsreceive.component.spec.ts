import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsreceiveComponent } from './smsreceive.component';

describe('SmsreceiveComponent', () => {
  let component: SmsreceiveComponent;
  let fixture: ComponentFixture<SmsreceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsreceiveComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsreceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
