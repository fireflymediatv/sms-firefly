import {TemplateRef, ViewChild, Component, OnInit, OnDestroy, OnChanges, DoCheck, ChangeDetectionStrategy } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { SmsreceiveService } from '../../services/smsreceive.service';
import { ParticipantService } from '../../services/participant.service';
import { SmscreditService } from '../../services/smscredit.service';
import {SelectionModel} from '@angular/cdk/collections';
import { ExportToCsv } from 'export-to-csv';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NbDateService } from '@nebular/theme';
import { NbThemeService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');
import { SurveyCampaignService} from '../../services/surveycampaign.service';
import { ExportToCSV } from '@molteni/export-csv';
import { Router } from '@angular/router';
const userRole = 'user';
export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

export interface UserSms {
  telephone: string;
  date: string;
  short_code: string;
  message: string;
}

const ELEMENT_DATA: any[] = [];

@Component({
  selector: 'ngx-smsreceive',
  templateUrl: './smsreceive.component.html',
  styleUrls: ['./smsreceive.component.scss'],
  providers : [SmsreceiveService, SurveyCampaignService],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SmsreceiveComponent implements OnInit, OnDestroy {
      options: any = {};
      public configuration: Config;
      public columns: Columns[];
      themeSubscription: any;
      route;
      smsinfo;
      data_sms;
      submitted = false;
      surveycampaigns;
      submittedSurvey;
      smsValue;
      smsCount;
      data_sms_csv;
      data_sms_chart;
      data_map_chart;
      chartConfigAudience: Object;
      dataSourceAudience: {};
      dateSmsForm: FormGroup;
      smsForm: FormGroup;
      dateValue;
      submittedSms = false;
      surveyForm: FormGroup;
      participantForm: FormGroup;
      creditForm: FormGroup;
      maxDate: Date;
      min: Date;
      loading_sms = false;
      public periodTitle = ['jour', 'semaine', 'mois'];
      max: Date;
      submittedCredit = false;
      submittedDate = false;
      public show: boolean = false;
      public buttonName: any = 'Ajouter';
      status: boolean = false;
      currentUser;
      static parameters = [SmsreceiveService, SurveyCampaignService, ParticipantService, FormBuilder, NbDateService];

      constructor(  private smsreveiceservice: SmsreceiveService,
                    private surveycampaignservice: SurveyCampaignService,
                    private participantservice: ParticipantService,
                    private smscreditService : SmscreditService,
                    private fb: FormBuilder,
                    private dateService: NbDateService<Date>,
                    private datePipe: DatePipe,
                    private theme: NbThemeService,
                    private router: Router) {

         this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // refresh without reload
         this.dateSmsForm = this.fb.group({
          data_date_sms : ['', Validators.required],
         });
         this.creditForm = this.fb.group({
                    credit : ['', Validators.required]
                   });

         this.surveyForm = this.fb.group({
          survey_campaign_id : ['', Validators.required],
         });

         this.participantForm = this.fb.group({
          stat : ['', Validators.required],
          period : ['', Validators.required],
         });
         //this.max = this.dateService.today();
         //this.min = this.dateService.addMonth(this.dateService.today(), -36);
      }

        // convenience getter for easy access to form fields
    get fDateProbe() {
      return this.dateSmsForm.controls;
    }

    // convenience getter for easy access to form fields
    get fSurvey() {
      return this.surveyForm.controls;
    }

     // convenience getter for easy access to form fields
    get fcreditProbe() {
        return this.creditForm.controls;
      }

    get fParticipant() {
      return this.participantForm.controls;
  }

  get fSms() {
    return this.smsForm.controls;
}


    ngOnInit(): void {
      this.currentUser = JSON.parse(localStorage.getItem('user'));
     if (this.currentUser.role != userRole) { // redirect page customer
      this.router.navigateByUrl(`/pages/customer/${this.currentUser.customerId}`);
      console.log('ici role');
    }
     // this.configuration.isLoading = true;
       this.columns = [
         { key: 'phone_number', title: 'Téléphone' },
         { key: 'receive_date', title: 'Date' },
         { key: 'short_code', title: 'Code court' },
         { key: 'campaign', title: 'Campagne' },
         { key: 'special', title: 'Message' },
       ];
       this.configuration = { ...DefaultConfig };
       this.configuration.isLoading = true;
       this.configuration.searchEnabled = true;
       this.getSmsreceive();
       this.getSurveyCampaigns();
       //this.smsInfo()
    }


        // Submit interval Date
  onSubmitDateInterval() {
          let date;
          this.submittedDate = true;
          if (this.dateSmsForm.invalid) {
            console.log('error date');
            return this.dateSmsForm.controls;
          }
          date = {
            'start_date' : this.datePipe.transform(this.fDateProbe.data_date_sms.value.start, 'yyyy-MM-dd'),
              'end_date' : this.datePipe.transform(this.fDateProbe.data_date_sms.value.end, 'yyyy-MM-dd')
          };
         this.getSmsByDate(date);
    }

    // Submit Campaign
    onSubmitSurvey() {
          this.submittedSurvey = true;
          if (this.surveyForm.invalid) {
            console.log('error survey form');
            return this.surveyForm.controls; ;
          }
          // console.log(' Id campaign ', this.fSurvey.survey_campaign_id.value)
          this.getSmsChart(this.fSurvey.survey_campaign_id.value);
      }

      // Show All Sms
    public getSmsreceive() {
      this.smsreveiceservice.getSmsreceive().subscribe((data: Array<object>) => {
        const count = data.length - 1;
         // console.log(' Sms receive data, see one', data);
         this.configuration.isLoading = false;
         this.data_sms  = data;
      //   this.getSmsChart("campaign_id");
      });
    }

      // Show All Sms
      public getSmsChart(obj_id) {
        this.smsreveiceservice.getSmsChart({'id_campaign': obj_id}).subscribe((data: Array<object>) => {
           this.data_sms_chart = data[0]; // list of msg
           this.data_map_chart = data[1]; // msg && count
           console.log('--getSmsChart value :', this.data_map_chart, this.data_sms_chart);
           this.getChart();
        });
      }

    public getSmsByDate(date_select) {
      this.smsreveiceservice.getSmsByDate(date_select).subscribe((data: Array<object>) => {
        // console.log(' response select dat, see one.. ', data[0]);
        this.data_sms  = data;
     });
    }

    public spentSms(val: any) {
      this.smsreveiceservice.spentSms(val).subscribe((data: Array<object>) => {
        this.smsValue  = data[0];
        this.smsCount = data[1];
        this.chartSpentSms();
     });
    }

    onSubmitSpentSms() {
      var dateobject;
      this.submittedSms = true;
      this.loading_sms = true;
      if (this.participantForm.invalid) {
        console.log('error date');
        return this.participantForm.controls;
      }
      dateobject = {
          'campaign' : this.fParticipant.stat.value,
          'start_date' : this.datePipe.transform(this.fParticipant.period.value.start, 'yyyy-MM-dd'),
          'end_date' : this.datePipe.transform(this.fParticipant.period.value.end, 'yyyy-MM-dd'),
      };
      this.spentSms(dateobject);
      this.loading_sms = false;
    }

    chartSpentSms() {
      // console.log('enter chart', this.residenceRepartition);
       // fusion chart
       this.chartConfigAudience = {
        width: '500',
        // height: '200',
        type: 'column2d',
        dataFormat: 'json',
     };
    
     this.dataSourceAudience = {
        'chart': {
         caption: 'Répartition des SMS dépensés pour la qualification',
         subcaption: '',
         yaxisname: '',
         xaxisname: 'Période',
         forceaxislimits: '1',
         pixelsperpoint: '0',
         pixelsperlabel: '30',
         compactdatamode: '1',
         theme: 'fusion',
        },
        'data': this.smsValue,
      };
     }

     // Show All Survey campaigns
  public getSurveyCampaigns() {
    this.surveycampaignservice.getSurveyCampaign().subscribe((data: Array<object>) => {
       // console.log(' survey campaign services', data);
       this.surveycampaigns = data;
    });
  }

       // download CSV

    downloadCSV() {
      let exporter = new ExportToCSV();
      this.smsreveiceservice.exportSmsToCsv('campaign_id').subscribe((data: Array<object>) => {
       // console.log(' data to download csv ', data);
        this.data_sms_csv  = data;
        exporter.exportColumnsToCSV(this.data_sms_csv, 'CampaignSmsToCsv', ['phone_number', 'message', 'campaign', 'receive_date', 'short_code']);
     });
    }

    getChart() {
      // console.group('-- entrer Chart--');
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        const colors = config.variables;
        const echarts: any = config.variables.echarts;

        this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: this.data_sms_chart,
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Message',
              type: 'pie',
              radius: '80%',
              center: ['50%', '50%'],
              data: this.data_map_chart,
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        };
      });
    }

    ngOnDestroy(): void {
      console.log('--OnDestrooy---');
      if (this.themeSubscription)
        this.themeSubscription.unsubscribe();
     //  this.router.navigateByUrl('/auth/login');
    }


 onSubmitCredit() {
          this.submittedCredit = true;
          if (this.creditForm.invalid){
            console.log('error add credit')
            return this.creditForm.controls;
          };
          let credit = this.fcreditProbe.credit.parent.value;
          console.log('error add credit', credit)
         this.Addcreditsms(credit)
      }
      
      // get Sms 
        smsInfo(){
          this.smscreditService.indexOne().subscribe((data: Array<object>) => {
            this.smsinfo = data
         });
       }

       public Addcreditsms(credit){
              this.smscreditService.create(credit).subscribe((data: Array<object>) => {
               this.fcreditProbe.credit.reset();
                this.smsInfo()
             });
           }
    }

